#include "block.h"
#include <iostream>

Block::Block(GameRoom* r)
{
    this->currentRoom = r;
}

Block::~Block()
{

}

void Block::initialize(const vector<sf::Vector2f>& vertices)
{

    // Build polygon according to provided vertices
    std::cout << "Block created using vertices, number : " << vertices.size() << std::endl;

    // Setup SFML shape
	sfPolygon.setPointCount(vertices.size());
    for (int i=0; i<vertices.size(); i++)
    {
        sfPolygon.setPoint(i, vertices[i]);
    }
    if (drawDebug) {
        sfPolygon.setFillColor(Color(255, 0, 0, 128));
    } else {
        sfPolygon.setFillColor(Color::White);
    }
    sfPolygon.setOutlineColor(Color::Black);
    sfPolygon.setOutlineThickness(1);

    isInitialized = true;
}

void Block::draw(RenderWindow * w)
{
    if (!isInitialized)
    {
        return;
    }
    w->draw(sfPolygon);
}

