#ifndef OTHERPLAYER_H
#define OTHERPLAYER_H

#include "gameobject.h"
#include "weapon.h"

class Bullet;

class OtherPlayer : public GameObject
{
    friend class GameRoom;

public:
    virtual void update(Time);
    virtual void draw(RenderWindow *);
    virtual RectangleShape getHitbox() { return hitbox; }
    void handleNetCom(int16_t pNetCom);
    void applyMovement();
    //b2Vec2 computeRunReaction(const b2Vec2& checkVector);
    void bleed(float xOffset, float yOffset, float bulletAngle);
    void setIsDead(bool pIsDead);
    bool getIsDead() { return isDead; }
    void insertBullet(uint, Bullet*);
    void removeBullet(uint pBulletIndex);

    short mpId = -1;
    map<WeaponType, Weapon> weapons;
    WeaponType currentWeapon = eUsp45;
    float weaponDirection = 0.f;
    float gunXOffset = 16;
    float gunYOffset = 14;
    RectangleShape hitbox = RectangleShape(Vector2f(16, 32));
    float hitboxXOffset = 8;
    float hitboxYOffset = 0;
    map<SoundIndex, Sound> sounds;
    map<uint, Bullet*> bullets;
    //b2Body* body;

protected:
    OtherPlayer(GameRoom*);
    ~OtherPlayer();

private:
    bool isDead = false;
    bool isGoingLeft = false;
    bool isGoingRight = false;
    bool isJumping = false;
};

#endif // OTHERPLAYER_H
