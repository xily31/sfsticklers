#include "promptmanager.h"
#include "TGUI/TGUI.hpp"
#include "defines.h"

PromptManager::PromptManager()
{
    myGui = new tgui::Gui();

    tgui::EditBox::Ptr editBox = tgui::EditBox::create();
    editBox.get()->setSize(200, 32);
    editBox.get()->setPosition(GAME_WIDTH/2 - editBox.get()->getSize().x/2,
                               GAME_HEIGHT/2 - editBox.get()->getSize().y/2);
	editBox.get()->setFocusable(true);
    editBox.get()->setFocused(true);
    myGui->add(editBox, "editBox");
    tgui::Label::Ptr questionLabel = tgui::Label::create("Enter a value");
    questionLabel.get()->setPosition(editBox.get()->getPosition().x,
                                     editBox.get()->getPosition().y - editBox.get()->getSize().y);
    myGui->add(questionLabel, "questionLabel");
}

// Sets isTyping to true, allowing the user to type stuff
bool PromptManager::prompt(string question, string defaultValue)
{
    if (!isTyping)
    {
        isTyping = true;
        tgui::Label::Ptr questionLabel = myGui->get<tgui::Label>("questionLabel");
        questionLabel.get()->setText(question);
        tgui::EditBox::Ptr editBox = myGui->get<tgui::EditBox>("editBox");
        editBox.get()->setFocused(true);
        editBox.get()->setText(defaultValue);
        return true;
    }
    return false;
}

// Handle editBox events
void PromptManager::handleEvent(Event e)
{
    if (isTyping)
    {
        myGui->handleEvent(e);
        char newChar = static_cast<char>(e.text.unicode);
        if (newChar == confirmChar)
        {
        }
    }
}

// Returns the string currently being typed, disables typing, clears prompter buffer
string PromptManager::getResult()
{
    isTyping = false;
    tgui::EditBox::Ptr editBox = myGui->get<tgui::EditBox>("editBox");
    return editBox.get()->getText();
}

// Displays the string being typed, and the question, if present
void PromptManager::draw(RenderWindow* w)
{
    if (isTyping)
    {
        if (!myGui->getTarget()) {
            myGui->setTarget(*w);
        }
        myGui->draw();
    }
}

void PromptManager::draw(RenderWindow* w, const View& v)
{
    if (!myGui->getTarget()) {
        myGui->setTarget(*w);
    }
    myGui->setView(v);
    draw(w);
}
