#ifndef ANIMSPRITE_H
#define ANIMSPRITE_H

//#include <QList>
#include "SFML/Graphics/Texture.hpp"
#include "SFML/System/Time.hpp"
#include "SFML/Graphics/RenderWindow.hpp"
#include "SFML/Audio.hpp"
#include <string>
#include <vector>
#include <map>
#include "defines.h"

using namespace sf;
using namespace std;

class AnimSprite
{
public:
    AnimSprite();
    ~AnimSprite();
    void updateImage(Time);
    bool addImageFromFile(string fileName);
    bool addImagesFromTileSet(string fileName, ushort tileSize, ushort posX, ushort posY, ushort length);
    void draw(RenderWindow*, float x, float y);
    void replay(int indexFrom = 0);

    float imageSpeed = 24;
    float imageIndex = 0.f;
    float imageXScale = 1.f;
    float imageYScale = 1.f;
    float lastTopImageIndex = 0;
    Vector2f origin = Vector2f(0,0);
    float angle = 0;
    vector<Texture> images;
    map<ushort, Sound> animSounds; // <Frame, Sound (sfml)>
    bool isLoop = true;
    bool isOver = false;
};

#endif // ANIMSPRITE_H
