#include "animsprite.h"
#include "SFML/Graphics/Sprite.hpp"
#include "resloader.h"
#include <iostream>
#include <math.h>
//#include <QDir>
//#include <QDebug>

AnimSprite::AnimSprite()
{

}

AnimSprite::~AnimSprite()
{
}

// Play animated sprite
void AnimSprite::updateImage(Time t)
{
    float prevImageIndex = imageIndex;
    if (isLoop || (!isLoop && !isOver)) {
        imageIndex += imageSpeed * t.asSeconds();
        for (auto& frameSound : animSounds) { // Play anim sounds
            if ((prevImageIndex < frameSound.first) && (imageIndex >= frameSound.first)) {
                frameSound.second.play();
            }
        }
    }
    if (imageIndex >= images.size())
    {
        if (isLoop) {
            imageIndex -= images.size();
        } else {
            imageIndex = lastTopImageIndex;
        }
        isOver = true;
    }
}

// Replays animation from provided int indexFrom
void AnimSprite::replay(int indexFrom)
{
    imageIndex = indexFrom;
    if (int((imageIndex) >= images.size()))
    {
        imageIndex = 0;
    }
    isOver = false;
}

// Adds an image from provided fileName to the images[] array
bool AnimSprite::addImageFromFile(string fileName)
{
    bool bRet = true;
    Texture t;
    bRet &= t.loadFromFile(fileName);
    images.push_back(t);
    return bRet;
}

// Adds one or more images from provided tileset fileName according to the provided tileSize, posX, posY, and anim length
bool AnimSprite::addImagesFromTileSet(string fileName, ushort tileSize, ushort posX, ushort posY, ushort length)
{
    bool bRet = true;
    for (ushort i=0; i<length; i++)
    {
        Texture t;
        bRet &= t.loadFromFile(fileName, sf::IntRect(posX*tileSize+i*tileSize, posY*tileSize, tileSize, tileSize));
        images.push_back(t);
    }
    if (!bRet) {
        images.clear();
    }
    return bRet;
}

// Draw method override
void AnimSprite::draw(RenderWindow* w, float x, float y)
{
    if (int(imageIndex) >= images.size()) return; // Avoid out of range crashes
    Sprite s((images.at(int(imageIndex))));
    s.setScale(imageXScale, imageYScale);
    float xOffset = (imageXScale < 0 ? s.getTextureRect().width * imageXScale : 0);
    s.setPosition(x-xOffset, y);
    s.setOrigin(origin);
    s.setRotation(angle);
    w->draw(s);
}
