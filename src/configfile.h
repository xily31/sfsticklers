#ifndef __CONFIG_FILE_H__
#define __CONFIG_FILE_H__

#include <string>
#include <map>

using namespace std;

class ConfigFile {
    std::map<std::string,string> content_;
    std::string mFileName;

public:
    ConfigFile(std::string const& configFile);

    string const& Value(std::string const& section, std::string const& entry) const;
    void setValue(std::string const& section, std::string const& key, std::string const& value);
    void save();
};

#endif
