#ifndef MAINMENU_H
#define MAINMENU_H

#include "gameobject.h"


namespace tgui
{
    class Gui;
}

class MainMenu : public GameObject
{
    friend class GameRoom;
    virtual void handleEvent(Event);
    virtual void draw(RenderWindow *);

public:
    void onHostButtonPressed();
    void onJoinButtonPressed();
    void onEditorButtonPressed();
	void onOptionsButtonPressed();
	void onEditKeyButtonPressed();
	void onBackToMenuButtonPressed();
	void onKeySelected();
    virtual ~MainMenu();

protected:
    MainMenu(GameRoom* r);

private:
    Font arialFont;
    Text menuText;
    tgui::Gui* mainGui;
	tgui::Gui* optionsGui;
	tgui::Gui* currentGui;
	bool isEditingKey = false;
};

#endif // MAINMENU_H
