#include "server.h"
//#include <QDebug>
#include "defines.h"
#include <iostream>
#include <algorithm>
#include "vtools.h"
#include <TGUI/TGUI.hpp>

Server::Server(GameRoom* r)
{
    arialFont.loadFromFile("res/fonts/arial.ttf");
    this->currentRoom = r;
    if (serverSocket.listen(tcpPort) != sf::Socket::Done)
    {
        cout << "Server can't listen on port " << tcpPort << endl;
    }
    else
    {
        cout << "Server listening on port " << tcpPort << endl;
        isOpen = true;
        serverSocket.setBlocking(false);
        setupUdpPorts();
    }

    view.setSize(GAME_WIDTH, GAME_HEIGHT);
    view.setCenter(GAME_WIDTH/2, GAME_HEIGHT/2);
    setPosition(Vector2f(GAME_WIDTH/2, GAME_HEIGHT/2));
    view.setViewport(FloatRect(0,0,1,1));


	mapLoaderGui = new tgui::Gui();
	mapLoaderGui->setTarget(*currentRoom->window);
	mapLoaderGui->setView(view);
	

	tgui::ListBox::Ptr mapList = tgui::ListBox::create();
	mapList.get()->setSize(200, (32 * mapList.get()->getItemHeight() * 10) % GAME_HEIGHT);
	mapList.get()->setPosition(10, 42);
	mapList.get()->connect("doubleclicked", &Server::onMapSelected, this);
	mapLoaderGui->add(mapList, "mapList");
	vector<string> mapFiles = VTools::listDirFiles("stm");
	for (string mapFile : mapFiles) {
		mapList.get()->addItem(mapFile);
	}

	tgui::Label::Ptr mapListLabel = tgui::Label::create("Map list");
	mapListLabel.get()->setPosition(10, 10);
	mapLoaderGui->add(mapListLabel, "mapListLabel");

	tgui::Label::Ptr statusLabel = tgui::Label::create("Waiting...");
	statusLabel.get()->setAutoSize(true);
	statusLabel.get()->setPosition(GAME_WIDTH - 10 - statusLabel.get()->getSize().x, 10);
	mapLoaderGui->add(statusLabel, "statusLabel");

	view.setCenter(Vector2f(0, 0));
	currentRoom->window->setView(view);
	cout << "Server initialised" << endl;
}

void Server::onMapSelected()
{
	tgui::ListBox::Ptr mapList = mapLoaderGui->get<tgui::ListBox>("mapList");
	string mapName = (string)mapList.get()->getSelectedItem();
	cout << "map selected : " << mapName << endl;
	string mapFileName = mapName;
	GameRoom* r = ResLoader::loadMap(mapFileName, currentRoom->window);
	if (r == NULL) {
		cout << "Loaded map is null" << mapFileName << endl;
		return;
	}
	cout << "Loading new map..." << endl;
	currentRoom->roomToSwitchTo = r;
	transferToRoom(r);
	mapLoaderGui->setOpacity(0);
}

void Server::draw(RenderWindow * w)
{
}

void Server::handleEvent(Event e)
{
	if (mapLoaderGui->getOpacity() > 0) {
		mapLoaderGui->handleEvent(e);
	}
	
    switch (e.type)
    {
    case Event::TextEntered:
    {
        char newChar = static_cast<char>(e.text.unicode);
		if (newChar == 'l' && mapLoaderGui->getOpacity() == 0)
		{
			mapLoaderGui->setOpacity(1);
        }
        break;
    }
	default:
		break;
    }
}

void Server::update(Time t)
{
    // Manage disconnected players
    auto itEnd = remotePlayers.end();
    for (auto it=remotePlayers.begin(); it!=itEnd; ++it)
    {
        RemotePlayer* r = *it;
        if (r) {
            if (r->isDisconnected) {
                removeRemotePlayer(r);
            }
        }
    }

    // Accept incoming connections
    SocketHolder* socketHolder = new SocketHolder;
    if (serverSocket.accept(socketHolder->tcpSocket) == sf::Socket::Done)
    {
        RemotePlayer* newPlayer = new RemotePlayer(currentRoom, socketHolder);
        currentRoom->addExistingGameObject<RemotePlayer>(newPlayer);
        // Detect new player
        cout << "Incoming connection!" << endl;
        Packet initPacket;
        newPlayer->udpReceivePort = reserveUdpPort();
        newPlayer->udpSendPort = reserveReceiveUdpPort();
        // Reserve udp port for new player
        if (newPlayer->udpReceivePort == 0 || newPlayer->udpSendPort == 0)
        {
            cout << "Server failed to provide udp ports, aborting" << endl;
            newPlayer->destroyLater = true;
            return;
        }
        // Reserve mp id for new player
        newPlayer->mpId = reserveMpId();
        // Bind new player's udp socket to newly reserved udp port
        newPlayer->udpSocket.bind(newPlayer->udpReceivePort);
        newPlayer->remoteIp = newPlayer->socketHolder->tcpSocket.getRemoteAddress().toString().c_str();
        // Add new player to players array
        newPlayer->setPosition(Vector2f(400, 300));
        newPlayer->server = this;
        remotePlayers.push_back(newPlayer);
        newPlayer->isPersistent = true;
        cout << "new player joined, ipaddr : " << newPlayer->tcpSocket->getRemoteAddress().toString().c_str();

        // Tell the new player his mpid and remote udp port
        short packetId = NETID_INITPACKET;
        cout << "sending mpId : " << newPlayer->mpId << ", udpsend port : " << newPlayer->udpReceivePort << ", udp rcv port : " << newPlayer->udpSendPort << endl;
        initPacket << packetId << newPlayer->mpId << newPlayer->udpReceivePort << newPlayer->udpSendPort;
        newPlayer->tcpSocket->send(initPacket);

        // Tell others a new player has come
        packetId = NETID_PLAYERJOIN;
        short otherMpId = newPlayer->mpId;
        Packet playerJoinedPacket;
        playerJoinedPacket << packetId << otherMpId;
        for (auto it=remotePlayers.begin(); it!=remotePlayers.end(); ++it)
        {
            RemotePlayer* r = *it;
            if (r == newPlayer) continue;
            r->sendTcp(playerJoinedPacket);

            view.setCenter(r->getPosition());
            currentRoom->window->setView(view);
        }

        // Tell new player who is already here
        // --> done after at the end of map content packet
    }
    else
    {
        delete socketHolder;
    }
}

RemotePlayer* Server::getRemotePlayerByMpId(short rpMpId)
{
    for (auto it=remotePlayers.begin(); it!=remotePlayers.end(); ++it)
    {
        RemotePlayer* r = *it;
        if (r->mpId == rpMpId) return r;
    }
    cout << "RemotePlayer not found" << rpMpId << endl;
    return NULL;
}

void Server::setupUdpPorts()
{
    for (int i=0; i<NET_UDPSERVERPORTSRANGE; i++)
    {
        // TODO : check if its not broken
        udpPortsInUse.insert(pair<unsigned short, bool>(i+NET_UDPSERVERPORTSSTART, false));
    }
}

unsigned short Server::reserveUdpPort()
{
    for (int i=NET_UDPSERVERPORTSSTART; i<(NET_UDPSERVERPORTSSTART+NET_UDPSERVERPORTSRANGE); i++)
    {
        if (udpPortsInUse[i] == false)
        {
            udpPortsInUse[i] = true;
            return i;
        }
    }
    return 0;
}

unsigned short Server::reserveReceiveUdpPort()
{
    for (int i=NET_UDPSERVERPORTSSTART+NET_UDPCLIENTPORTSRANGE;
         i<(NET_UDPSERVERPORTSSTART+NET_UDPSERVERPORTSRANGE+NET_UDPCLIENTPORTSRANGE);
         i++)
    {
        if (udpClientPortsInUse[i] == false)
        {
            udpClientPortsInUse[i] = true;
            return i;
        }
    }
    return 0;
}

void Server::removeRemotePlayer(RemotePlayer* r)
{
    freeUdpPort(r->udpReceivePort);
    freeMpId(r->mpId);
    VTools::removeAll(remotePlayers, r);
    r->destroyLater = true;
}

void Server::freeUdpPort(unsigned short port)
{
    udpPortsInUse[port] = false;
}

void Server::freeUdpClientPort(unsigned short port)
{
    udpClientPortsInUse[port] = false;
}

void Server::setupMpIds()
{
    for (int i=0; i<GAME_MAXPLAYERS; i++)
    {
        mpIdsInUse.insert(pair<ushort, bool>(i, false));
    }
}

unsigned short Server::reserveMpId()
{
    for (int i=0; i<GAME_MAXPLAYERS; i++)
    {
        if (mpIdsInUse[i] == false)
        {
            mpIdsInUse[i] = true;
            return i;
        }
    }
    return 0;
}

void Server::freeMpId(unsigned short mpId)
{
    mpIdsInUse[mpId] = false;
}

void Server::drawUi(RenderWindow* w)
{
	string qText = (isOpen? "Listening" : "Can't listen");
	for (auto it=remotePlayers.begin(); it!=remotePlayers.end(); ++it)
	{
		RemotePlayer* r = *it;
		qText += "\nRemotePlayer, mpId : " + to_string(r->mpId) + ", udpPort : " + to_string(r->udpReceivePort);
	}
	tgui::Label::Ptr statusLabel = mapLoaderGui->get<tgui::Label>("statusLabel");
	if (!statusLabel.get() || !mapLoaderGui)
	{
		wcout << "statusLabel or mapLoaderGui is null ! Exiting Server::drawUi" << endl;
		return;
	}
	statusLabel.get()->setText(qText);
	statusLabel.get()->setPosition(GAME_WIDTH - 10 - statusLabel.get()->getSize().x, 10);
	if (prompter.getIsTyping()) prompter.draw(w);
	mapLoaderGui->draw();
}
