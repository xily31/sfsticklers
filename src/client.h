#ifndef CLIENT_H
#define CLIENT_H

#include "gameobject.h"
#include "SFML/Network.hpp"
#include "player.h"
#include "otherplayer.h"
//#include <QString>
#include <string>

using namespace std;

class Player;

class Client : public GameObject
{
    friend class GameRoom;

public:
    bool connect(string ip, unsigned short port);
    virtual void handleEvent(Event);
    virtual void draw(RenderWindow *);
    virtual void update(Time);
    void sendTcp(Packet);
    void sendUdp(Packet);
    OtherPlayer* getOtherPlayerByMpId(short);

    TcpSocket tcpSocket;
    UdpSocket udpSocket;
    bool isConnected = false;
    short mpId = -1;
    string serverIp = "127.0.0.1";
    unsigned short serverTcpPort = 6510;
    unsigned short serverReceiveUdpPort = 0;
    unsigned short clientReceiveUdpPort = 0;
    Player* localPlayer = NULL;

protected:
    Client(GameRoom*);

private:
    void receiveData();

    Font arialFont;
};

#endif // CLIENT_H
