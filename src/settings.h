#ifndef SETTINGS_H
#define SETTINGS_H

#include "defines.h"
#include <map>
#include <SFML/Window/Keyboard.hpp>
#include <string>

class Settings
{
public:
	std::map<eGameKey, sf::Keyboard::Key> gameKeys;
	std::string serverIp = "127.0.0.1";

	static Settings& getInstance();
	bool readSettings();
	void writeSettings();

protected:
	static Settings* mInstance;

	Settings();

private:
	sf::Keyboard::Key readKey(std::string keyName);
};

#endif // SETTINGS_H
