#include "gameobject.h"
//#include "qdebug.h"

GameObject::GameObject(GameRoom* r)
{
    currentRoom = r;
}

GameObject::~GameObject()
{
}

void GameObject::draw(RenderWindow* w)
{
    sprites[spriteIndex].draw(w, position.x, position.y);
}

void GameObject::drawUi(RenderWindow* w)
{
}

void GameObject::update(Time)
{

}

void GameObject::handleEvent(Event e)
{

}

void GameObject::setPosition(Vector2f p)
{
    this->position = p;
    this->hitbox.setPosition(p);
}

void GameObject::setHitbox(Vector2f hitbox)
{
    this->hitbox = RectangleShape(hitbox);
    this->hitbox.setPosition(position);
}

void GameObject::transferToRoom(GameRoom* r)
{
    r->addExistingGameObject<GameObject>(this);
    this->currentRoom = r;
}

Vector2f GameObject::getMousePos()
{
    return currentRoom->window->mapPixelToCoords(Mouse::getPosition(*(currentRoom->window)));
}
