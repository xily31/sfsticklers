#include "configfile.h"

#include <fstream>
#include <iostream>
#include "vtools.h"

using namespace std;

std::string trim(std::string const& source, char const* delims = " \t\r\n")
{
    std::string result(source);
    std::string::size_type index = result.find_last_not_of(delims);
    if(index != std::string::npos)
    result.erase(++index);

    index = result.find_first_not_of(delims);
    if(index != std::string::npos)
    result.erase(0, index);
    else
    result.erase();
    return result;
}

ConfigFile::ConfigFile(std::string const& configFile)
    : mFileName(configFile)
{
    std::ifstream file(configFile.c_str());
    std::string line;
    std::string name;
    std::string value;
    std::string inSection;
    int posEqual;
    while (std::getline(file,line))
    {
        if (! line.length()) continue;

        if (line[0] == '#') continue;
        if (line[0] == ';') continue;

        if (line[0] == '[') {
            inSection=trim(line.substr(1,line.find(']')-1));
            continue;
        }

        posEqual=line.find('=');
        name  = trim(line.substr(0,posEqual));
        value = trim(line.substr(posEqual+1));

        content_[inSection+'/'+name]=value;
    }
}

string const& ConfigFile::Value(std::string const& section, std::string const& entry) const
{
    std::map<std::string,string>::const_iterator ci = content_.find(section + '/' + entry);

    if (ci == content_.end()) throw "does not exist";

    return ci->second;
}

void ConfigFile::setValue(std::string const& section, std::string const& key, std::string const& value)
{
    std::string sectionKey = section + "/" + key;
    content_[sectionKey] = value;
}

void ConfigFile::save()
{
    std::string& fileName = mFileName;
    string dirName = "./";
    ofstream file;
    file.open(fileName, ios_base::out | ios_base::in);
    if (file.is_open())
    {
        cout << "Overwriting map file" + dirName + fileName << endl;
    }
    else
    {
        file.close();
        file.open(fileName, ios_base::out);
    }

    if ( file.is_open() )
    {
        std::string lastSection = "";
        for (auto pair : content_)
        {
            std::vector<string> vec = VTools::split(pair.first, '/');
            std::string section = vec[0];
            std::string key = vec[1];
            std::string value = pair.second;
            if (lastSection == "" || lastSection != section)
            {
                // write [section]
                file << "[" << section << "]" << endl;
            }
            // write key=value
            file << key << "=" << value << endl;
            lastSection = section;
        }
    }
    else
    {
        cout << "Could not open file " << fileName << " for writing" << endl;
    }
}
