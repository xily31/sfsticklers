#include "player.h"
#include "block.h"
#include "vtools.h"
#include "bullet.h"
#include "visualfx.h"
#include <iostream>
#include "particle.h"
#include "settings.h"

Player::Player(GameRoom* r)
{
	this->currentRoom = r;

    ResLoader* rl = ResLoader::getInstance();
    // Load sprites
    sprites[sStand] = rl->getSprite(sStand);
    sprites[sRun] = rl->getSprite(sRun);
    sprites[sRunBack] = rl->getSprite(sRunBack);
    sprites[sJump] = rl->getSprite(sJump);
    sprites[sFall] = rl->getSprite(sFall);
    sprites[sDeath] = rl->getSprite(sDeath);

    // Load sounds
    sounds[sdDie] = rl->getSound(sdDie);
    sounds[sdHitFlesh] = rl->getSound(sdHitFlesh);
    sounds[sdHeadshot] = rl->getSound(sdHeadshot);

    arialFont.loadFromFile("res/fonts/arial.ttf");

    setPosition(Vector2f(GAME_WIDTH/2, GAME_HEIGHT/2));

    weapons[currentWeapon] = Weapon::getWeapon(eUsp45);
}

Player::~Player()
{
    std::cout << "Player destructor" << std::endl;
}

size_t Player::addBullet(Bullet* pBullet)
{
    bullets.push_back(pBullet);
    pBullet->mpId = bullets.size() - 1;
    return pBullet->mpId;
}

void Player::removeBullet(Bullet* pBullet)
{
    VTools::removeAll(bullets, pBullet);
}

// Applies world physics
void Player::applyWorldPhysics(Time t)
{
    ySpeed += gravity * t.asMicroseconds() / 10000;
    if (ySpeed > 10) ySpeed = 10;
}


// Adjusts xSpeed and ySpeed according to collisions with blocks
void Player::checkCollisions()
{
    vector<Block*> worldBlocks = currentRoom->getAllObjects<Block>();
    isOnGround = false;
    float x = position.x + hitboxXOffset;
    float y = position.y + hitboxYOffset;
    float w = hitbox.getSize().x;
    float h = hitbox.getSize().y;
    for (auto it=worldBlocks.begin(); it!=worldBlocks.end(); ++it)
    {
        Block* b = *it;

        FloatRect blockRect = Rect<float>(b->getPosition().x, b->getPosition().y, b->getBounds().width, b->getBounds().height);
        FloatRect intersRectResult;

        if (ySpeed != 0)
        {
            float potY = y + ySpeed;
            FloatRect playerRect = Rect<float>(x, potY, w, h);
            if (blockRect.intersects(playerRect, intersRectResult))
            {
                if (ySpeed > 0) {
                    if (ySpeed > gravity) {
                        isMainPacketNeeded = true; // Update pos/speed when player lands on ground
                    }
                    isOnGround = true;
                    position.y += ySpeed - (potY + h - blockRect.top);
                } else {
                    position.y += ySpeed + (blockRect.top + blockRect.height - potY);
                }
                ySpeed = 0;
            }
        }
        if (xSpeed != 0)
        {
            float potX = x + xSpeed;
            FloatRect playerRect = Rect<float>(potX, y, w, h);
            if (blockRect.intersects(playerRect, intersRectResult))
            {
                if (xSpeed > 0)
                {
                    position.x += xSpeed - (potX + w - blockRect.left);
                }
                else
                {
                    position.x += xSpeed + (blockRect.left + blockRect.width - potX);
                }
                xSpeed = 0;
            }
        }
    }
}


// Handles generic event and channel it to other methods
void Player::handleEvent(Event e)
{
    // Do not handle events when player is dead
    if (isDead) return;

    switch (e.type)
    {
    case Event::KeyPressed:
        handleKeyPressed(e.key);
        break;

    case Event::KeyReleased:
        handleKeyReleased(e.key);
        break;

    case Event::MouseButtonPressed:
        handleMousePressed(e.mouseButton);
        break;

    default: return;
    }
}

// Handles mouse button pressed
void Player::handleMousePressed(Event::MouseButtonEvent e)
{
    if (e.button == Mouse::Left)
    {
        shoot();
    }
	else if (e.button == Mouse::Right)
	{
		//Sound space = ResLoader::getInstance()->getSound(sdUsp45Shoot);
		Sound& space = sounds[sdHeadshot];
		space.setRelativeToListener(true);
		//space.
		space.play();
	}
}

void Player::computeIsOnGround(float timeSecs)
{
    isSliding = false;

    // Decrease ableToRun timer
    if (ableToRunTimer > 0) {
        ableToRunTimer -= timeSecs;
        if (ableToRunTimer <= 0) {
            ableToRunTimer = 0;
        }
    }
}

void Player::setIsDead(bool pIsDead)
{
    isDead = pIsDead;
    if (isDead)
    {
        sounds[sdDie].play();
        clearKeys();
        spriteIndex = sDeath;
        sprites[spriteIndex].replay();
    }
    else
    {
        //sounds[sdRespawn].play(); // Respawn sound
        // todo : move player to respawn position
    }
}

void Player::setHealth(int pHealth)
{
    health = pHealth;
    /*if (health <= 0)
    {
        isDead = true;
    }*/
}

// Updated event
void Player::update(const Time t)
{
    SpriteIndex previousSpriteIndex = spriteIndex;
    bool previousIsTurnedRight = (sprites[spriteIndex].imageXScale == 1);
    int previousWeaponAngle = (int)weaponDir;
    float previousImageIndex = sprites[spriteIndex].imageIndex;
    WeaponType previousWeaponType = currentWeapon;
    Weapon::WeaponSprite previousWeaponSprite = weapons[currentWeapon].currentSprite;

    // Compute whether the player is on ground
    //computeIsOnGround(t.asSeconds());

    // Handle keys that are being held
    handleKeysHeld();
	
	// Apply world physics properties, eg : gravity, speed limitation
    applyWorldPhysics(t);
	
    // Check for collision with blocks
    checkCollisions();
	
	// Move according to computed xSpeed and ySpeed, after collisions
    position.x += xSpeed * t.asMicroseconds() / 10000;
    position.y += ySpeed * t.asMicroseconds() / 10000;
    xSpeedPrevious = xSpeed;
    ySpeedPrevious = ySpeed;

    // Update weapon direction
    Weapon& w = weapons[currentWeapon];
    weaponDir = -1 * VTools::pointDirection(position + Vector2f(0, -w.barrelHeight) + Vector2f(gunXOffset, gunYOffset), getMousePos());
    w.angle = weaponDir;
    w.update(t);

    // Update player sprite according to input, speed, and direction
    updateSprite();
    sprites[spriteIndex].updateImage(t);

    // Update screen shake
    if (screenShakeRate > 0)
    {
        screenShakeOffset = Vector2f(VTools::lengthDirX(screenShakeDirection, screenShakeRate),
                                     VTools::lengthDirY(screenShakeDirection, screenShakeRate));
        screenShakeRate -= screenShakeRecovery * t.asSeconds();
        if (screenShakeRate < 0) {
            screenShakeRate = 0;
            screenShakeOffset = Vector2f(0, 0);
        }
    }

    // Check if we need to update serverside graphic state
    isGraphicStatePacketNeeded |= previousIsTurnedRight != (sprites[spriteIndex].imageXScale == 1);
    isGraphicStatePacketNeeded |= previousSpriteIndex != spriteIndex;
    isGraphicStatePacketNeeded |= previousWeaponAngle != (int)(weaponDir);
    isGraphicStatePacketNeeded |= previousWeaponSprite != weapons[currentWeapon].currentSprite;
    isGraphicStatePacketNeeded |= (previousImageIndex != sprites[spriteIndex].imageIndex) && isDead;

    // If connected, send graphic update packet every 1/20 s
    if (client != NULL)
    {
        lastNetUpdateTime += t.asSeconds();
        if (lastNetUpdateTime >= 0.05f)
        {
            if (isGraphicStatePacketNeeded) {
                isGraphicStatePacketNeeded = false;
                sendGraphicStatePacket();
            }
			if (!(isDead && position.y >= currentRoom->height))
			{
				sendMainPacket(-1);
			}
            lastNetUpdateTime = 0;
        }
    }
    else // If not connected, handle respawn when outside map locally
    {
        // respawn at respawn position
    }
}

void Player::shoot()
{
    Weapon& w = weapons[currentWeapon];

    if (!w.shoot()) return;

    Bullet* b = currentRoom->createGameObject<Bullet>();
    float aimDirectionRad = VTools::degToRad(w.angle);
    Vector2f gunOffset = Vector2f(gunXOffset+VTools::lengthDirX(aimDirectionRad, w.barrelOffset),
                                  gunYOffset-w.barrelHeight+VTools::lengthDirY(aimDirectionRad, w.barrelOffset)
                                  );
    b->setPosition(position+gunOffset);
    b->setMotion(-w.angle, 20);
    b->playerObj = this;
    screenShakeRate = 16;
    screenShakeDirection = aimDirectionRad;

    // Muzzle flash
    VisualFX* fx = currentRoom->createGameObject<VisualFX>();
    fx->setPosition(b->getPosition());
    fx->init(sMuzzleFlash, -b->getDirection());

    if (client) {
        b->shooterMpId = client->mpId;
        short sendId = NETID_SHOT;
        float sendX = b->getPosition().x;
        float sendY = b->getPosition().y;
        float sendVelocity = 0;
        float sendDirection = 0;
        uint sendBulletMpId = addBullet(b);
        Packet bulletPacket;
        bulletPacket << sendId << sendBulletMpId << sendX << sendY << sendVelocity << sendDirection;
        client->sendUdp(bulletPacket);
    }
}

// Handles keys pressed
void Player::handleKeyPressed(Event::KeyEvent k)
{
	Settings& settings = Settings::getInstance();

    if (std::find(keysHeld.begin(), keysHeld.end(), k.code) == keysHeld.end())
    {
        keysHeld.push_back(k.code);

		if (k.code == settings.gameKeys[eGameKey::Left])
        {
            sendMainPacket(NETCOM_GOLEFT);
        }
		else if (k.code == settings.gameKeys[eGameKey::Right])
        {
			sendMainPacket(NETCOM_GORIGHT);
        }
		else if (k.code == settings.gameKeys[eGameKey::Up])
        {
            if (isOnGround && !isSliding) {
                sendMainPacket(NETCOM_JUMP);
			}
        }
		else if (k.code == settings.gameKeys[eGameKey::Reload])
        {
			//cout << "Reload pressed" << endl;
            Weapon& weap = weapons[currentWeapon];
            if
                (weap.reload())
            {
                this->isGraphicStatePacketNeeded = true;
			}
        }
    }
}

// Handles keys released
void Player::handleKeyReleased(Event::KeyEvent k)
{
	Settings& settings = Settings::getInstance();
    VTools::removeAll(keysHeld, k.code);

	if (k.code == settings.gameKeys[eGameKey::Left])
    {
        xSpeed = 0;
		sendMainPacket(NETCOM_STOPGOLEFT);
    }
	else if (k.code == settings.gameKeys[eGameKey::Right])
    {
        xSpeed = 0;
		sendMainPacket(NETCOM_STOPGORIGHT);
    }
	else if (k.code == settings.gameKeys[eGameKey::Up])
    {
		sendMainPacket(NETCOM_STOPJUMP);
	}
}

void Player::clearKeys()
{
    keysHeld.clear();
}

// Called every cycle, checks for keys being held
void Player::handleKeysHeld()
{
	Settings& settings = Settings::getInstance();
    for (auto it=keysHeld.begin(); it!=keysHeld.end(); ++it)
    {
        Keyboard::Key k = *it;
		if (k == settings.gameKeys[eGameKey::Left])
        {
            xSpeed = -4;
        }
		else if (k == settings.gameKeys[eGameKey::Right])
        {
            xSpeed = 4;
		}
		else if (k == settings.gameKeys[eGameKey::Up])
		{
			if (isOnGround) {
                ySpeed = -6;
            }
		}
    }
}

// Draw event
void Player::draw(RenderWindow* w)
{

    // Draw player sprite
    sprites[spriteIndex].draw(w, position.x, position.y);

    // Draw weapon
    Weapon& weap = weapons[currentWeapon];
    if (!isDead) {
        weap.draw(w, position.x+gunXOffset, position.y+gunYOffset);
        weap.sprites[weap.currentSprite].angle = weaponDir;
    }

    // Draw player center + xOffset
//    CircleShape c(4);
//    c.setOrigin(2, 2);
//    c.setPosition(position + Vector2f(gunXOffset, gunYOffset));
//    w->draw(c);

    // Update view
	currentRoom->view.setCenter(position + screenShakeOffset);
	//currentRoom->window->setView(view);
}

void Player::drawUi(RenderWindow * w)
{
    // Draw health
    Text hud;
    string hudText = "Health : " + to_string(health);
    hud.setString(String(hudText));
    hud.setPosition(currentRoom->window->mapPixelToCoords(Vector2i(10, 10)));
    hud.setColor(Color::Black);
    hud.setFont(arialFont);
    w->draw(hud);

    // Draw crosshair (disk for now)
    CircleShape c = CircleShape(8);
    c.setPosition(getMousePos());
    c.setFillColor(Color::Red);
    c.setOrigin(8, 8);
    w->draw(c);
}

// Show the right sprite according to speed, keys held, and aiming
void Player::updateSprite()
{
    float mouseX = getMousePos().x;

    if (!isDead)
    {
        if (mouseX > position.x)
            sprites[spriteIndex].imageXScale = 1;
        else
            sprites[spriteIndex].imageXScale = -1;

        if (isOnGround)
        {
            if (int(xSpeed) != 0) // TODO : put back round instead of int()
            {
                if ((sprites[spriteIndex].imageXScale > 0 && xSpeed > 0) || (sprites[spriteIndex].imageXScale < 0 && xSpeed < 0))
                    spriteIndex = sRun;

                else
                    spriteIndex = sRunBack;
            }
            else
            {
                spriteIndex = sStand;
            }
        }
        else
        {
            if (ySpeed > 0)
                spriteIndex = sJump;
            else
                spriteIndex = sFall;
        }
    }
}

void Player::sendMainPacket(int16_t pNetCom = -1)
{
    if (client == NULL) {
        return;
    }
    Packet p;
    int16_t mId = NETID_MAINPACKET;
    int16_t moveId = pNetCom;
    float netX = 0;//body->GetPosition().x;
    float netY = 0;//body->GetPosition().y;
    float netXSpeed = 0;//body->GetLinearVelocity().x;
    float netYSpeed = 0;//body->GetLinearVelocity().y;
    p << mId << moveId << netX << netY << netXSpeed << netYSpeed;
    client->sendUdp(p);
}

void Player::sendGraphicStatePacket()
{
    if (client == NULL) {
        return;
    }
    //cout << rand() << " sending graphic state" << endl;
    Packet p;
    int16_t mId = NETID_GRAPHICSTATE;
    int32_t intSpriteIndex = (int32_t)spriteIndex;
    int32_t intWeaponSpriteIndex = (int32_t)weapons[currentWeapon].currentSprite;
    bool isTurnedRight = (sprites[spriteIndex].imageXScale == 1);
    int16_t netAngle = (int16_t)weaponDir;
    p << mId << intSpriteIndex << isTurnedRight << netAngle << intWeaponSpriteIndex;
    client->sendUdp(p);
}

void Player::bleed(float xOffset, float yOffset, float bulletAngle)
{
    Vector2f potPos(position.x + xOffset, position.y + yOffset);
    for (int i=0; i<20; ++i)
    {
        Particle* gib = currentRoom->createGameObject<Particle>();
        gib->init(sBlood);
        int generalDirection = bulletAngle;
        if ((generalDirection > 0 && generalDirection < 90) || generalDirection > 270) {
            generalDirection += 20;
        } else if (generalDirection > 90 && generalDirection < 180) {
            generalDirection -= 20;
        }
        gib->setPosition(potPos);
        gib->setMotion(3, 5, generalDirection - 10, generalDirection + 10);
    }
    if (yOffset < GAME_HEADSHOTOFFSET) { // Headshot
        for (int i=0; i<20; ++i)
        {
            Particle* gib = currentRoom->createGameObject<Particle>();
            gib->init(sGibs);
            int generalDirection = bulletAngle + 360;
            generalDirection %= 360;
            if ((generalDirection > 0 && generalDirection < 90) || generalDirection > 270) {
                generalDirection = 60;
            } else if (generalDirection > 90 && generalDirection < 180) {
                generalDirection = 120;
            }
            gib->setPosition(potPos);
            gib->setMotion(3, 5, generalDirection - 10, generalDirection + 10);
        }
    }
}
