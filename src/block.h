#ifndef BLOCK_H
#define BLOCK_H

#include "gameobject.h"

class Block : public GameObject
{
    friend class GameRoom;

public:
    FloatRect getBounds() { return hitbox.getLocalBounds(); }
    // Sets isInitialized to true and sets up physics engine

    void initialize(const vector<sf::Vector2f>& vertices);
    bool getIsInitialized() { return isInitialized; }

    // Define and display Box2D objects
    sf::ConvexShape sfPolygon;

protected:
    Block(GameRoom*);
    ~Block();
    virtual void draw(RenderWindow *);

private:
    bool isInitialized = false;
};

#endif // BLOCK_H
