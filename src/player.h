#ifndef PLAYER_H
#define PLAYER_H

#include "gameobject.h"
#include "animsprite.h"
#include "weapon.h"
#include "client.h"
#include "resloader.h"
#include "block.h"

class Bullet;
class Client;
class Settings;

class Player : public GameObject
{
    friend class GameRoom;

public:
    void computeIsOnGround(float timeSecs);
    bool getIsOnGround() { return isOnGround; }
    virtual RectangleShape getHitbox() { return hitbox; }
    //b2Body* getBody() { return body; }
    void bleed(float xOffset, float yOffset, float bulletAngle);
    void setHealth(int pHealth);
    void setIsDead(bool pIsDead);
    size_t addBullet(Bullet*);
    void removeBullet(Bullet*);

    Client* client = NULL;
    SpriteIndex spriteIndex = sStand;
    bool isDead = false;
    float hitboxXOffset = 8;
    float hitboxYOffset = 0;
	void clearKeys();
    float screenShakeRate = 0; // View pixel offset
    float screenShakeRecovery = 128; // View pixel offset per second
    float screenShakeDirection = 0; // Degrees
    Vector2f screenShakeOffset;
    unsigned short groundContacts = 0;
    bool isGraphicStatePacketNeeded = false;
    map<SoundIndex, Sound> sounds;
    vector<Bullet*> bullets;

protected:
    Player(GameRoom*);
    virtual ~Player();
    virtual void draw(RenderWindow *);
    virtual void drawUi(RenderWindow *);
    virtual void update(Time);
    virtual void handleEvent(Event);

private:
    void handleKeyPressed(Event::KeyEvent);
    void handleKeyReleased(Event::KeyEvent);
    void handleKeysHeld();
    void handleMousePressed(Event::MouseButtonEvent);
    void applyWorldPhysics(Time);
    void checkCollisions();
    void updateSprite();
    void sendMainPacket(int16_t pNetCom);
    void sendGraphicStatePacket();
    void shoot();
    //b2Vec2 computeGroundReaction();
    //b2Vec2 computeRunReaction(const b2Vec2&);
    //void run(const b2Vec2& direction, float speed);

    vector<Keyboard::Key> keysHeld;
    float weaponDir = 0.f;
    bool isOnGround = false;
    bool isSliding = false;
    bool isAbleToRun = true;
    float ableToRunTimer = 0;
    float gunXOffset = 16;
    float gunYOffset = 14;
    map<SpriteIndex, AnimSprite> sprites;
    map<WeaponType, Weapon> weapons;
    WeaponType currentWeapon = eUsp45;
    float lastNetUpdateTime = 0;
    Font arialFont;
    RectangleShape hitbox = RectangleShape(Vector2f(16, 32));
    int health = 100;
	float xSpeed = 0;
    float ySpeed = 0;
    float xSpeedPrevious = 0;
    float ySpeedPrevious = 0;
	float gravity = 0.2;
	bool isMainPacketNeeded = false;
};

#endif // PLAYER_H
