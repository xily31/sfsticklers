#ifndef RESLOADER_H
#define RESLOADER_H

#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "animsprite.h"
#include "gameroom.h"
#include "defines.h"
#include <map>
#include <string>

using namespace std;

class GameRoom;

class ResLoader
{
private:
    ResLoader();
    void loadSprites();
    void loadSounds();

    static ResLoader* instance;
    map<SpriteIndex, AnimSprite> sprites;
    map<SoundIndex, SoundBuffer> sounds;

public:         
    static ResLoader* getInstance()
    {
        if (!instance) instance = new ResLoader;
        return instance;
    }
    AnimSprite getSprite(SpriteIndex);
    Sound getSound(SoundIndex);
    static GameRoom* loadMap(string fileName, RenderWindow* w);

    bool isLoaded = false;
};

#endif // RESLOADER_H
