#include "otherplayer.h"
#include "vtools.h"
#include "particle.h"
#include "bullet.h"

OtherPlayer::OtherPlayer(GameRoom* r)
{
    this->currentRoom = r;
    ResLoader* rl = ResLoader::getInstance();

    // Load sprites
    sprites[sStand] = rl->getSprite(sStand);
    sprites[sRun] = rl->getSprite(sRun);
    sprites[sRunBack] = rl->getSprite(sRunBack);
    sprites[sJump] = rl->getSprite(sJump);
    sprites[sFall] = rl->getSprite(sFall);
    sprites[sDeath] = rl->getSprite(sDeath);
    setSpriteIndex(sStand);

    // Load sounds
    sounds[sdDie] = ResLoader::getInstance()->getSound(sdDie);
    sounds[sdHitFlesh] = ResLoader::getInstance()->getSound(sdHitFlesh);
    sounds[sdHeadshot] = ResLoader::getInstance()->getSound(sdHeadshot);

    // Init weapon objects
    weapons[eUsp45] = Weapon::getWeapon(eUsp45);
    /*for (auto weapon : weapons)
    {
        weapon.second.isLocalPlayer = false;
    }*/
    weapons[eUsp45].isLocalPlayer = false;
    currentWeapon = eUsp45;

    // Define the dynamic body. We set its position and call the body factory.
    /*b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(0.0f, 4.0f);
    bodyDef.fixedRotation = true;
    body = r->world->CreateBody(&bodyDef);
	body->SetGravityScale(0); // Let local clients simulate

    // Box fixture
    b2PolygonShape dynamicBox;
    dynamicBox.SetAsBox(.25f, .255f);
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &dynamicBox;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.0f;
    fixtureDef.filter.categoryBits = B2dFamily::eWorldDynamic;
    fixtureDef.filter.maskBits = B2dFamily::eWorldStatic;
    body->CreateFixture(&fixtureDef);
    body->SetUserData(this);
    // Circle fixture, bottom
    b2CircleShape dynamicCircle;
    dynamicCircle.m_radius = .25f;
    dynamicCircle.m_p.y = -.25f;
    b2FixtureDef circleFixture;
    circleFixture.friction = 0.f;
    circleFixture.filter.categoryBits = B2dFamily::eWorldDynamic;
    circleFixture.filter.maskBits = B2dFamily::eWorldStatic;
    circleFixture.shape = &dynamicCircle;
    body->CreateFixture(&circleFixture);
    // Circle fixture, top
    b2CircleShape topCircleShape;
    topCircleShape.m_radius = .25f;
    topCircleShape.m_p.y = +.25f;
    b2FixtureDef topCircleFixture;
    topCircleFixture.friction = 0.f;
    topCircleFixture.shape = &topCircleShape;
    topCircleFixture.filter.categoryBits = B2dFamily::eWorldDynamic;
    topCircleFixture.filter.maskBits = B2dFamily::eWorldStatic;
    body->CreateFixture(&topCircleFixture);*/
}

OtherPlayer::~OtherPlayer()
{
    //currentRoom->world->DestroyBody(body);
}

void insertBullet(uint, Bullet*);
void removeBullet(Bullet*);

void OtherPlayer::insertBullet(uint pIndex, Bullet* pBullet)
{
    bullets[pIndex] = pBullet;
}

void OtherPlayer::removeBullet(uint pBulletIndex)
{
	if (bullets.size() > 0) {
		if (bullets[pBulletIndex]) {
			bullets[pBulletIndex]->destroyLater = true;
			bullets[pBulletIndex] = NULL;
		}
	}
}

void OtherPlayer::update(Time t)
{
    // Anim sprite
    sprites[spriteIndex].updateImage(t);

    // Update weapon
    Weapon* w = &weapons[currentWeapon];
    w->angle = weaponDirection;
    w->update(t);

    // Move according to received player input
    applyMovement();

    // Move according to computed Box2D data
    /*position.x = body->GetPosition().x*PPM-PPM/2;
    position.y = -body->GetPosition().y*PPM-PPM/2;*/
}

void OtherPlayer::bleed(float xOffset, float yOffset, float bulletAngle)
{
    sounds[sdHitFlesh].play();
    Vector2f potPos(position.x + xOffset, position.y + yOffset);
    for (int i=0; i<20; ++i)
    {
        Particle* gib = currentRoom->createGameObject<Particle>();
        gib->init(sBlood);
        int generalDirection = bulletAngle;
        if ((generalDirection > 0 && generalDirection < 90) || generalDirection > 270) {
            generalDirection += 20;
        } else if (generalDirection > 90 && generalDirection < 180) {
            generalDirection -= 20;
        }
        gib->setPosition(potPos);
        gib->setMotion(3, 5, generalDirection - 10, generalDirection + 10);
    }
    if (yOffset < GAME_HEADSHOTOFFSET)  // Headshot
    {
        sounds[sdHeadshot].play();
        for (int i=0; i<20; ++i)
        {
            Particle* gib = currentRoom->createGameObject<Particle>();
            gib->init(sGibs);
            int generalDirection = bulletAngle + 360;
            generalDirection %= 360;
            if ((generalDirection > 0 && generalDirection < 90) || generalDirection > 270) {
                generalDirection = 120;
            } else if (generalDirection > 90 && generalDirection < 180) {
                generalDirection = 60;
            }
            gib->setPosition(potPos);
            gib->setMotion(3, 5, generalDirection - 10, generalDirection + 10);
        }
    }
}

void OtherPlayer::handleNetCom(int16_t pNetCom)
{
    switch (pNetCom)
    {
    case NETCOM_GOLEFT: {
        isGoingLeft = true;
        break;
    }

    case NETCOM_STOPGOLEFT: {
        isGoingLeft = false;
        break;
    }

    case NETCOM_GORIGHT: {
        isGoingRight = true;
        break;
    }

    case NETCOM_STOPGORIGHT: {
        isGoingRight = false;
        break;
    }

    case NETCOM_JUMP: {
        /*b2Vec2 velocity = body->GetLinearVelocity();
        velocity.y = 20;
        body->SetLinearVelocity(velocity);*/
        isJumping = true; // isJumping not used yet
        break;
    }

    case NETCOM_STOPJUMP: {
        isJumping = false;
        break;
    }
    }
}

void OtherPlayer::setIsDead(bool pIsDead)
{
    isDead = pIsDead;
    /*body->SetActive(!pIsDead);
    body->SetAwake(!pIsDead);*/
    if (pIsDead) {
        sounds[sdDie].play();
        spriteIndex = sDeath;
        sprites[spriteIndex].replay();
    } else {
        //sounds[sdRespawn].play();
    }
}

/*b2Vec2 OtherPlayer::computeRunReaction(const b2Vec2& checkVector)
{
    b2Vec2 ret(0, 0);
    float lowestFraction = 1;
    for (b2Body* b = currentRoom->world->GetBodyList(); b; b = b->GetNext()) {
        if (b != body)
        {
            for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) // TODO : check only blocks in the vicinity
            {
                b2RayCastInput ri;
                b2RayCastOutput ro;
                ri.maxFraction = 1;
                ri.p1 = body->GetPosition() + b2Vec2(0, -.45); // Check from bottom of the player
                ri.p2 = ri.p1 + checkVector;
                if (f->RayCast(&ro, ri, 0))
                {
                    float fraction = ro.fraction;
                    if (fraction < lowestFraction)
                    {
                        ret = ro.normal;
                        lowestFraction = fraction;
                    }
                }
            }
        }
    }
    return ret;
}*/

void OtherPlayer::applyMovement()
{
    if (isGoingLeft && !isGoingRight) // TODO : refactor from Player
    {
	/*
        float runRatio = 1;
        b2Vec2 runReaction = computeRunReaction(b2Vec2(-.5, 0));
        if (abs(runReaction.x) > abs(runReaction.y))
            runRatio = 0;
        if (runRatio != 0)
        {
            b2Vec2 velocity = body->GetLinearVelocity();
            velocity.x = -10;
            body->SetLinearVelocity(velocity);
        }
	*/
    }

    if (isGoingRight && !isGoingLeft)
    {
        float runRatio = 1;
        /*
	b2Vec2 runReaction = computeRunReaction(b2Vec2(.5, 0));
        if (abs(runReaction.x) > abs(runReaction.y))
            runRatio = 0;
        if (runRatio != 0)
        {
            b2Vec2 velocity = body->GetLinearVelocity();
            velocity.x = 10;
            body->SetLinearVelocity(velocity);
        }
*/
    }
}

void OtherPlayer::draw(RenderWindow * w)
{
    sprites[spriteIndex].draw(w, position.x, position.y);

    if (!isDead) {
        Weapon* weapon = &weapons[currentWeapon];
        weapon->sprites[weapon->currentSprite].draw(w, position.x+gunXOffset, position.y+gunYOffset);
    }

    /*if (drawDebug)
    {
        for (b2Fixture* f = body->GetFixtureList(); f; f = f->GetNext())
        {
            b2PolygonShape* b2PolyShape = dynamic_cast<b2PolygonShape*>(f->GetShape());
            if (b2PolyShape)
            {
                sf::ConvexShape sfPolygon;
                sfPolygon.setPointCount(b2PolyShape->m_count);
                for (int i=0; i<b2PolyShape->m_count; i++)
                {
                    b2Vec2 point = b2PolyShape->m_vertices[i];
                    b2Vec2 absPoint = body->GetWorldPoint(point);
                    Vector2f pointPosition(
                                absPoint.x*PPM,
                                -absPoint.y*PPM
                                );
                    sfPolygon.setPoint(i, pointPosition);
                }
                sfPolygon.setFillColor(Color(255, 255, 255, 128));
                sfPolygon.setOutlineColor(Color::Black);
                sfPolygon.setOutlineThickness(1);
                w->draw(sfPolygon);
            }
            else
            {
                b2CircleShape* b2Circle = dynamic_cast<b2CircleShape*>(f->GetShape());
                if (b2Circle)
                {
                    float r = b2Circle->m_radius;
                    sf::CircleShape sfCircle = sf::CircleShape(r*PPM);
                    sfCircle.setOrigin(r*PPM, r*PPM);
                    sfCircle.setPosition(
                                PPM*(body->GetPosition().x + b2Circle->m_p.x),
                                -PPM*(body->GetPosition().y + b2Circle->m_p.y)
                                     );
                    sfCircle.setFillColor(Color(255, 255, 255, 128));
                    sfCircle.setOutlineColor(Color::Black);
                    sfCircle.setOutlineThickness(1);
                    w->draw(sfCircle);
                }
            }
        }
    }*/
}
