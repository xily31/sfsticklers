#ifndef PROMPTMANAGER_H
#define PROMPTMANAGER_H

#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
//#include <QString>
#include <string>

using namespace std;
using namespace sf;

namespace tgui {
    class Gui;
}

class PromptManager
{
public:
    PromptManager();
    bool prompt(string question, string defaultValue = "");
    void handleEvent(Event);
    void draw(RenderWindow* w);
    void draw(RenderWindow* w, const View& v);
    string getResult();
    bool getIsTyping() { return isTyping; }

private:
    bool isTyping = false;
    char confirmChar = '\r';
    tgui::Gui* myGui;
};

#endif // PROMPTMANAGER_H
