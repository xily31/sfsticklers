#include "particle.h"
#include "vtools.h"

Particle::Particle(GameRoom* r)
{
    this->currentRoom = r;
}

void Particle::init(SpriteIndex si, bool randomImage)
{
    ResLoader* rl = ResLoader::getInstance();
    sprites[si] = rl->getSprite(si);
    spriteIndex = si;
    AnimSprite& currentSprite = sprites[si];
    if (randomImage)
    {
        currentSprite.imageSpeed = 0;
        currentSprite.imageIndex = currentSprite.images.size() * VTools::random();
    }
}

Particle::~Particle()
{

}

void Particle::setMotion(float speed, float direction)
{
    this->speed = speed;
    this->direction = 180 + direction;
    xSpeed = VTools::lengthDirX(VTools::degToRad(direction), speed);
    ySpeed = VTools::lengthDirY(VTools::degToRad(direction), speed);
}

void Particle::setMotion(float speedMin, float speedMax, float dirMin, float dirMax)
{
    this->speed = speedMin + speedMax * VTools::random();
    this->direction = 180 + dirMin + (dirMax-dirMin) * VTools::random();
    xSpeed = VTools::lengthDirX(VTools::degToRad(direction), speed);
    ySpeed = VTools::lengthDirY(VTools::degToRad(direction), speed);
}

void Particle::update(Time t)
{
    ySpeed += gravity * t.asMicroseconds() / 10000;
    position.x += xSpeed * t.asMicroseconds() / 10000;
    position.y += ySpeed * t.asMicroseconds() / 10000;

    lifeSpan -= t.asSeconds();
    if (lifeSpan <= 0)
    {
        this->destroyLater;
    }
}

/*void Particle::draw(RenderWindow *r)
{

}
*/
