#ifndef SERVER_H
#define SERVER_H

#include "gameroom.h"
#include "SFML/Network.hpp"
#include "remoteplayer.h"
//#include <QMap>
#include "promptmanager.h"
#include <vector>

using namespace std;

class RemotePlayer;

namespace tgui {
	class Gui;
}

class Server : public GameObject
{

    friend class GameRoom;
    friend class RemotePlayer;

public:
    Server(GameRoom*);
    virtual void draw(RenderWindow *);
    virtual void update(Time);
    virtual void handleEvent(Event);
	virtual void drawUi(RenderWindow *);
    void removeRemotePlayer(RemotePlayer*);

    TcpListener serverSocket;
    vector<RemotePlayer*> remotePlayers;
    unsigned short tcpPort = 6510;
    bool isOpen = false;
    Font arialFont;
    RemotePlayer* getRemotePlayerByMpId(short);
	void onMapSelected();

private:
    void setupUdpPorts(); // Init udpPortsInUse QMap
    unsigned short reserveUdpPort(); // Returns reserved send (client->server) port. 0 if failed.
    unsigned short reserveReceiveUdpPort(); // Returns reserved receive (server->client) port. 0 if failed.
    void freeUdpPort(unsigned short); // Frees udp port.
    void freeUdpClientPort(unsigned short); // Frees udp client receive port.
    void setupMpIds(); // Init mpIdsInUse QMap
    unsigned short reserveMpId(); // Returns reserved mpid. 0 if failed.
    void freeMpId(unsigned short); // Frees mp id.

    map<unsigned short, bool> udpPortsInUse; // false means not in use
    map<unsigned short, bool> udpClientPortsInUse; // false means not in use
    map<unsigned short, bool> mpIdsInUse; // false means not in use
    PromptManager prompter;
    View view;
	tgui::Gui* mapLoaderGui;
};

#endif // SERVER_H
