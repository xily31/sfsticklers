#ifndef PLAYERGROUNDCONTACTLISTENER_H
#define PLAYERGROUNDCONTACTLISTENER_H

// Ground collision callback, must be included from player.cpp, after dependencies
class PlayerGroundContactListener : public b2ContactListener
{
    // Helper function to figure out if the collision was between
    // the player and a static block, and sort out which is which
    bool getPlayerSensorAndBlock(b2Contact* contact, Player*& playerEntity, Block*& blockEntity)
	{
        b2Fixture* fixtureA = contact->GetFixtureA();
        b2Fixture* fixtureB = contact->GetFixtureB();
        // Make sure only one of the fixtures was a sensor
        bool sensorA = fixtureA->IsSensor();
        bool sensorB = fixtureB->IsSensor();
        if ( ! (sensorA ^ sensorB) ) {
            return false;
        }
        if (sensorA) {
            playerEntity = static_cast<Player*>(fixtureA->GetBody()->GetUserData());
            blockEntity = static_cast<Block*>(fixtureB->GetBody()->GetUserData());
        } else {
            playerEntity = static_cast<Player*>(fixtureB->GetBody()->GetUserData());
            blockEntity = static_cast<Block*>(fixtureA->GetBody()->GetUserData());
        }
        return true;
    }

    void BeginContact(b2Contact* contact) {
        Player* playerEntity;
        Block* blockEntity;
        if ( getPlayerSensorAndBlock(contact, playerEntity, blockEntity) )
        {
            playerEntity->groundContacts++;
            //playerEntity->isMainPacketNeeded = true;
        }
    }

    void EndContact(b2Contact* contact) {
        Player* playerEntity;
        Block* blockEntity;
        if ( getPlayerSensorAndBlock(contact, playerEntity, blockEntity) )
        {
            playerEntity->groundContacts--;
            //playerEntity->isMainPacketNeeded = true;
        }
    }
};

#endif
