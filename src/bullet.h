#ifndef BULLET_H
#define BULLET_H

#include "gameobject.h"
#include "player.h"

class Bullet : public GameObject
{
    friend class GameRoom;

public:
    virtual void update(Time);
    void setMotion(float direction, float velocity);
    float getDirection() { return direction; }
    float getVelocity() { return velocity; }

    short shooterMpId = -1;
    Player* playerObj = NULL;
    float lifeSpan = 2; // in seconds
    float lifeCounter = 0; // in seconds
    uint mpId;

protected:
    Bullet(GameRoom*);
    virtual ~Bullet();

    float direction = 0.f;
    float velocity = 0.f;
};

#endif // BULLET_H
