#include "visualfx.h"
#include "resloader.h"

VisualFX::VisualFX(GameRoom* r)
{
    this->currentRoom = r;
}

void VisualFX::init(SpriteIndex s, float angle)
{
    this->spriteIndex = s;
    this->sprites[spriteIndex] = ResLoader::getInstance()->getSprite(s);
    this->sprites[spriteIndex].angle = angle;
}

void VisualFX::update(Time t)
{
    if (sprites[spriteIndex].isOver) {
        this->destroyLater = true;
    }
    sprites[spriteIndex].updateImage(t);
}

void VisualFX::draw(RenderWindow* rw)
{
    sprites[spriteIndex].draw(rw, position.x, position.y);
}
