#include "client.h"
//#include <QDebug>
#include "block.h"
#include "vtools.h"
#include "bullet.h"
#include <iostream>
#include "visualfx.h"


Client::Client(GameRoom* r)
{
    this->currentRoom = r;
    arialFont.loadFromFile("res/fonts/arial.ttf");
}

// Attempts to connect to a server with the provided ip and port. Returns a bool accordingly.
bool Client::connect(string ip, unsigned short port)
{
    serverIp = ip.c_str();
    serverTcpPort = port;
    Socket::Status status = tcpSocket.connect(serverIp.c_str(), serverTcpPort);
    if (status != sf::Socket::Done)
    {
        cout << "Could not connect on" << serverIp << ":" << serverTcpPort << endl;
        return false;
    }
    cout << "Connected to" << serverIp << ":"  << serverTcpPort << endl;
    VTools::iniWriteString("settings.ini", "NETWORK", "ServerIp", serverIp);
    isConnected = true;
    tcpSocket.setBlocking(false);
    udpSocket.setBlocking(false);

    return true;
}

void Client::handleEvent(Event e)
{
    switch (e.type)
    {
    case Event::Closed: // Send TCP leave message if window is closed
    {
        Packet p;
        short mId = NETID_TCPQUIT;
        p << mId;
        sendTcp(p);
        break;
    }

    case Event::KeyPressed:
    {
        if (e.key.code == Keyboard::C)
        {
            string ipToConnect = VTools::iniReadString("settings.ini", "NETWORK", "ServerIp");
            if (ipToConnect == "") ipToConnect = "127.0.0.1";
            cout << "Using ip " << ipToConnect << endl;
            connect(ipToConnect, NET_TCPPORT);
        }
        break;
    }

    default: return;
    }
}

void Client::update(Time t)
{
    if (isConnected)
        receiveData();
}

void Client::receiveData()
{
    if (!isConnected) return;
    Packet tcpPacket;
    if (tcpSocket.receive(tcpPacket) == TcpSocket::Done)
    {
        short messageId;
        tcpPacket >> messageId;
        switch (messageId)
        {
        case NETID_INITPACKET:
        {
            // Receive Mp id and remote udp port
            short newMpId;
            unsigned short newServerReceiveUdpPort;
            unsigned short newClientReceiveUdpPort;
            tcpPacket >> newMpId;
            tcpPacket >> newServerReceiveUdpPort;
            tcpPacket >> newClientReceiveUdpPort;
            this->mpId = newMpId;
            this->serverReceiveUdpPort = newServerReceiveUdpPort;
            this->clientReceiveUdpPort = newClientReceiveUdpPort;
            cout << "Received mp id :" << mpId << endl;
            cout << "Received server receive udp port :" << serverReceiveUdpPort << endl;
            cout << "Received client receive udp port :" << clientReceiveUdpPort << endl;
            udpSocket.bind(clientReceiveUdpPort);

            // Ask for map dimensions
            short sendId = NETID_REQMAP;
            Packet sendPacket;
            sendPacket << sendId;
            sendTcp(sendPacket);
            break;
        }

        case NETID_REQMAP:
        {
            // Receive map size
            float mapWidth, mapHeight;
            tcpPacket >> mapWidth >> mapHeight;
            GameRoom* newMap = new GameRoom(currentRoom->window);
            newMap->width = mapWidth;
            newMap->height = mapHeight;
            // Create map and go to it while staying persistent
            currentRoom->roomToSwitchTo = newMap;
            transferToRoom(newMap);
            cout << "Received map " << mapWidth << ", " << mapHeight << endl;

            // Ask for map contents (blocks, maybe players...)
            short sendId = NETID_REQMAPCONTENT;
            Packet sendPacket;
            sendPacket << sendId;
            sendTcp(sendPacket);
            break;
        }

        case NETID_NEWBLOCK:
        {
            // Receive a new block
            short verticesCount;
            tcpPacket >> verticesCount;
            if (verticesCount <= 0) {
                break;
            }
            vector<sf::Vector2f> newBlockVertices;
            for (int i=0; i<verticesCount; ++i)
            {
                float vertX, vertY;
                tcpPacket >> vertX >> vertY;
                newBlockVertices.push_back(Vector2f(vertX*PPM, -vertY*PPM));
            }
            Block* b = currentRoom->createGameObject<Block>();
            b->initialize(newBlockVertices);
            cout << "Received block, " << verticesCount << " vertices" << endl;
            break;
        }

        case NETID_REQMAPCONTENT:
        {
            cout << "Map is ready, spawning" << endl;
            // Map is ready to be played (fully sent)
            localPlayer = currentRoom->createGameObject<Player>();
            //localPlayer->getBody()->SetTransform(b2Vec2(0, 0), 0);
            localPlayer->client = this;
            break;
        }

        case NETID_DEATH:
        {
            short rcvMpId;
            tcpPacket >> rcvMpId;
            cout << "Player" << rcvMpId << "died" << endl;
            if (localPlayer && mpId == rcvMpId)
            {
                localPlayer->setIsDead(true);
                break;
            }
            OtherPlayer* o = getOtherPlayerByMpId(rcvMpId);
            if (o == NULL) break;
            o->setIsDead(true);
            break;
        }

        case NETID_SPAWN:
        {
            short rcvMpId;
            float rcvX, rcvY;
            tcpPacket >> rcvMpId >> rcvX >> rcvY;

            cout << "Player " << rcvMpId << "(re)spawned, " << rcvX << ", " << rcvY << endl;
            if (localPlayer && mpId == rcvMpId)
            {
                localPlayer->setIsDead(false);
                //localPlayer->getBody()->SetTransform(b2Vec2(rcvX/PPM, -rcvY/PPM), 0);
                break;
            }
            OtherPlayer* o = getOtherPlayerByMpId(rcvMpId);
            if (o == NULL) break;
            o->setIsDead(false);
            o->setPosition(Vector2f(rcvX, rcvY));
            break;
        }

        case NETID_PLAYERJOIN:
        {
            short newMpId;
            tcpPacket >> newMpId;
            cout << "New player joined the game, mpId : " << newMpId << endl;
            OtherPlayer* o = currentRoom->createGameObject<OtherPlayer>();
            if (o != NULL)
            {
                o->mpId = newMpId;
                o->setPosition(Vector2f(100, 100));
            }
            break;
        }

        case NETID_PLAYERQUIT:
        {
            short newMpId;
            tcpPacket >> newMpId;
            cout << "Player has left, mpId : " << newMpId << endl;
            OtherPlayer* o = getOtherPlayerByMpId(newMpId);
            if (o == NULL) break;
            o->destroyLater = true;
            break;
        }

        case NETID_HEALTH:
        {
            int rcvHealth;
            tcpPacket >> rcvHealth;
            cout << "Player new health" << rcvHealth << endl;
            if (localPlayer) {
                localPlayer->setHealth(rcvHealth);            }
            break;
        }

        case NETID_PLAYERHIT:
        {
            short rcvVictimId;
            float rcvHitYOffset;
            float rcvHitAngle;
            uint rcvBulletId;
            short rcvShooterId;
            tcpPacket >> rcvShooterId >> rcvVictimId >> rcvBulletId >> rcvHitYOffset >> rcvHitAngle;
            cout << "Player " << rcvVictimId << " hit" << endl;

            OtherPlayer* shooterPlayer = getOtherPlayerByMpId(rcvShooterId);
            if (shooterPlayer) {
                shooterPlayer->removeBullet(rcvBulletId);
            }

            if (mpId == rcvVictimId && localPlayer)
            {
                // Local player bleed
                localPlayer->bleed(0, rcvHitYOffset, rcvHitAngle);
            }
            else
            {
                OtherPlayer* o = getOtherPlayerByMpId(rcvVictimId);
                if (o == NULL) break;
                o->bleed(0, rcvHitYOffset, rcvHitAngle);
                o->removeBullet(rcvBulletId);
            }
            break;
        }

        }
    }

    Packet udpPacket;
    IpAddress addr(serverIp.c_str());
    unsigned short port = clientReceiveUdpPort;//NET_UDPCLIENTPORT;
    if (udpSocket.receive(udpPacket, addr, port) == UdpSocket::Done)
    {
        int16_t messageId;
        udpPacket >> messageId;
        switch (messageId)
        {
        case NETID_MAINPACKET:
        {
            int16_t playerId;
            float playerX, playerY, playerXSpeed, playerYSpeed;
            int16_t netCom;
            udpPacket >> playerId >> netCom >> playerX >> playerY >> playerXSpeed >> playerYSpeed;
            OtherPlayer* o = getOtherPlayerByMpId(playerId);
            if (o == NULL) break;
            //o->body->SetTransform(b2Vec2(playerX, playerY), 0);
            //o->body->SetLinearVelocity(b2Vec2(playerXSpeed, playerYSpeed));
            o->handleNetCom(netCom);
            break;
        }

        case NETID_GRAPHICSTATE:
        {
            int16_t playerId;
            int32_t intSpriteIndex;
            int32_t intWeaponSpriteIndex;
            bool isTurnedRight;
            int16_t weaponAngle;
            udpPacket >> playerId >> intSpriteIndex >> isTurnedRight >> weaponAngle >> intWeaponSpriteIndex;
            OtherPlayer* o = getOtherPlayerByMpId(playerId);
            if (o == NULL) break;
            SpriteIndex previousSpriteIndex = o->getSpriteIndex();
            o->setSpriteIndex(static_cast<SpriteIndex>(intSpriteIndex));
            if (o->getSpriteIndex() == sDeath && previousSpriteIndex != o->getSpriteIndex()) {
                o->sprites[o->getSpriteIndex()].replay();
            }
            o->sprites[o->getSpriteIndex()].imageXScale = (isTurnedRight? 1 : -1);
            Weapon& playerWeapon = o->weapons[o->currentWeapon];
            Weapon::WeaponSprite weaponSprite = (Weapon::WeaponSprite)intWeaponSpriteIndex;
            o->weaponDirection = weaponAngle;
            if (playerWeapon.currentSprite != weaponSprite)
            {
                playerWeapon.currentSprite = weaponSprite;
                playerWeapon.sprites[playerWeapon.currentSprite].replay();
            }
            break;
        }

        case NETID_SHOT:
        {
            short shooterMpId;
            float bX, bY, bVelocity, bDirection;
            uint bulletMpId;
            udpPacket >> shooterMpId >> bulletMpId >> bX >> bY >> bVelocity >> bDirection; // TODO : receive weapon type too
            if (shooterMpId == mpId) return;
            Bullet* b = currentRoom->createGameObject<Bullet>();
            b->setPosition(Vector2f(bX, bY));
            b->setMotion(bDirection, bVelocity);
            b->shooterMpId = shooterMpId;
            OtherPlayer* o = getOtherPlayerByMpId(shooterMpId);
            if (o == NULL) break;
            o->weapons[o->currentWeapon].shoot();
            o->insertBullet(bulletMpId, b);
            // Muzzle flash
            VisualFX* fx = currentRoom->createGameObject<VisualFX>();
            fx->setPosition(b->getPosition());
            fx->init(sMuzzleFlash, -b->getDirection());
            break;
        }
        }
    }
}

void Client::draw(RenderWindow * w)
{
    string qClientMessage = (isConnected? "Connected" : "Disconnected (press C to connect)");
    if (mpId != -1) qClientMessage += "\nMP ID : " + to_string(mpId);
    if (serverReceiveUdpPort != 0) qClientMessage += "\nRemote UDP Port : " + to_string(serverReceiveUdpPort);
    const char* clientMessage = qClientMessage.c_str();
    Text menuText(clientMessage, arialFont);
    menuText.setPosition(4, 4);
    menuText.setColor(Color::Black);
    menuText.setCharacterSize(12);
    w->draw(menuText);
}

void Client::sendTcp(Packet p)
{
    tcpSocket.send(p);
}

void Client::sendUdp(Packet p)
{
    if (serverReceiveUdpPort == 0) return;
    unsigned short portSend = serverReceiveUdpPort;
    IpAddress sendIpServ(serverIp.c_str());
    if (udpSocket.send(p, sendIpServ, portSend) != UdpSocket::Done)
    {
        cout << "unable to send udp packet to" << string(serverIp) << portSend << endl;
    }
}

OtherPlayer* Client::getOtherPlayerByMpId(short searchedMpId)
{
    vector<OtherPlayer*> otherPlayers = currentRoom->getAllObjects<OtherPlayer>();
    for (auto o : otherPlayers)
    {
        if (o != NULL) {
            if (o->mpId == searchedMpId) return o;
        }
    }
    return NULL;
}
