//#include <QDebug>
#include "SFML/Graphics.hpp"
#include "defines.h"
#include "gameroom.h"
#include "gameobject.h"
#include "player.h"
//#include <QThread>
#include "roomeditor.h"
#include "block.h"
#include "vtools.h"
#include "bullet.h"
#include "mainmenu.h"
#include "remoteplayer.h"
#include "resloader.h"
#include <iostream>
#include "TGUI/TGUI.hpp"
#include "settings.h"

using namespace std;
using namespace sf;

int main(int argc, char *argv[])
{
	cout << Settings::getInstance().gameKeys[eGameKey::Up] << endl;
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    RenderWindow window(VideoMode(GAME_WIDTH, GAME_HEIGHT), "Sticklers", Style::Default, settings);
//    RenderWindow window(VideoMode::getFullscreenModes().at(0), "Sticklers", Style::Fullscreen);
	//window.setVerticalSyncEnabled(true);

    ResLoader::getInstance();

    GameRoom* gameRoom = new GameRoom(&window);
//    GameRoom* gameRoom = ResLoader::loadMap("testmap.stm", &window);

//    Player* playerObject = gameRoom->createGameObject<Player>();
//    Block* groundBlock = gameRoom->createGameObject<Block>();
    MainMenu* mainMenu = gameRoom->createGameObject<MainMenu>();

//    groundBlock->setHitbox(Vector2f(800, 32));
//    groundBlock->setPosition(Vector2f(0, 418));
//    gameRoom->createGameObject<RoomEditor>();

    Clock clock;

    while (window.isOpen())
    {
        Event event;
        bool isCloseRequested = false;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
            case Event::Closed :
                gameRoom->handleEvent(event); // So client / server sends each other a TCP quit message
                window.close();
                isCloseRequested = true;
                break;

            default:
                gameRoom->handleEvent(event);
                break;
            }
        }

        Time frameTime = clock.restart();

        window.clear(Color(192, 192, 192));
        gameRoom->update(frameTime);
        gameRoom->draw(&window);
        gameRoom->drawUi(&window);
        gameRoom->purgeObjects();
        gameRoom->addObjects();
        window.display();

        if (gameRoom->roomToSwitchTo != NULL)
        {
            gameRoom = gameRoom->roomToSwitchTo;
            GameRoom::currentRoom = gameRoom;
        }
    }

    return 0;
}
