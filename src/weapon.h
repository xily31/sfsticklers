#ifndef WEAPON_H
#define WEAPON_H

#include "animsprite.h"
//#include <QMap>
#include "SFML/Audio.hpp"
#include "defines.h"
#include <map>

using namespace std;

enum WeaponType
{
    eUsp45
};

class Weapon
{
public:
    enum WeaponSprite {
        eIdleSprite,
        eShootSprite,
        eReloadSprite,
        eDeploySprite
    };

    enum WeaponSound {
        ShootSound,
        ReloadSound
    };

    Weapon(); // Used by maps
    static Weapon getWeapon(WeaponType);
    void draw(RenderWindow* w, float x, float y);
    void update(Time);
    bool shoot(); // Applies recoil and plays anim
    bool reload();

    bool isLocalPlayer = true;
    bool isReloading = false;
    int ammoMag = 12;
    int ammoStock = 50000;
    int ammoPerMag = 12;
    unsigned short barrelOffset = 0;
    unsigned short barrelHeight = 0;
    float recoil = 0;
    float maxRecoil = 45;
    float recoilPerShot = 10;
    float recoilRecovery = 45; // Per second
    float angle = 0;
    map<WeaponSprite, AnimSprite> sprites;
    WeaponSprite currentSprite = eIdleSprite;
    map<WeaponSound, Sound> sounds;

protected:

};

#endif // WEAPON_H
