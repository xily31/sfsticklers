#ifndef PARTICLE_H
#define PARTICLE_H

#include "gameobject.h"

class Particle : public GameObject
{
    friend class GameRoom;

public:
    virtual void update(Time);
    //virtual void draw(RenderWindow* r);
    void init(SpriteIndex si, bool randomImage = true);
    void setMotion(float speed, float direction);
    void setMotion(float speedMin, float speedMax, float dirMin, float dirMax);

    float speed = 0;
    float direction = 0;
    float gravity = .2f;
    float lifeSpan = 2; // Life span, in seconds
    float xSpeed = 0;
    float ySpeed = 0;

protected:
    Particle(GameRoom* r);
    virtual ~Particle();
};

#endif // PARTICLE_H
