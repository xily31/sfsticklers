#include "mainmenu.h"
//#include <QDebug>
#include "server.h"
#include "client.h"
#include "vtools.h"
#include "roomeditor.h"
#include <iostream>
#include "TGUI/TGUI.hpp"
#include "settings.h"

MainMenu::MainMenu(GameRoom* r)
{
    this->currentRoom = r;
    arialFont.loadFromFile("res/fonts/arial.ttf");
    /*menuText.setString("1 - Host server\n2 - Join as client\n3 - Room editor");
    menuText.setFont(arialFont);
    menuText.setPosition(10, 10);
    menuText.setColor(Color::Black);*/

	// Main gui instanciation
	mainGui = new tgui::Gui(*r->window);
	currentGui = mainGui;

	// Join button
    tgui::Button::Ptr joinButton = tgui::Button::create();
    joinButton.get()->setPosition(10, 10);
    joinButton.get()->connect("pressed", &MainMenu::onJoinButtonPressed, this);
    joinButton->setText("Join");
    mainGui->add(joinButton, "joinButton");
	// Host button
    tgui::Button::Ptr hostButton = tgui::Button::create();
    hostButton.get()->setPosition(10, 50);
    hostButton.get()->connect("pressed", &MainMenu::onHostButtonPressed, this);
    hostButton->setText("Host");
    mainGui->add(hostButton, "hostButton");
	// Editor button
    tgui::Button::Ptr editorButton = tgui::Button::create();
    editorButton.get()->setPosition(10, 90);
    editorButton.get()->connect("pressed", &MainMenu::onEditorButtonPressed, this);
    editorButton->setText("Editor");
    mainGui->add(editorButton, "editorButton");
	// Options button
	tgui::Button::Ptr optionsButton = tgui::Button::create();
	optionsButton.get()->setPosition(10, 130);
	optionsButton.get()->connect("pressed", &MainMenu::onOptionsButtonPressed, this);
	optionsButton->setText("Options");
	mainGui->add(optionsButton, "optionsButton");
	// Server IP line edit
    tgui::EditBox::Ptr ipEditBox = tgui::EditBox::create();
    ipEditBox.get()->setPosition(joinButton.get()->getPosition().x +
                                 joinButton.get()->getSize().x + 10,
                                 joinButton.get()->getPosition().y);
    ipEditBox.get()->setText(VTools::iniReadString("settings.ini", "NETWORK", "ServerIp"));
    ipEditBox.get()->setSize(200, joinButton.get()->getSize().y);
    mainGui->add(ipEditBox, "ipEditBox");

	// Option gui instanciation
	optionsGui = new tgui::Gui(*r->window);

	// Keys listbox
	tgui::ListBox::Ptr keysListBox = tgui::ListBox::create();
	keysListBox.get()->setPosition(10, 10);
	keysListBox.get()->addItem("Up");
	keysListBox.get()->addItem("Left");
	keysListBox.get()->addItem("Right");
	keysListBox.get()->addItem("Reload");
	keysListBox.get()->connect("ItemSelected", &MainMenu::onKeySelected, this);
	keysListBox.get()->setSelectedItem("Up");
	optionsGui->add(keysListBox, "keysListBox");
	// Edit key button
	tgui::Button::Ptr editKeyButton = tgui::Button::create();
	editKeyButton.get()->setPosition(
				keysListBox.get()->getPosition().x + keysListBox.get()->getSize().x + 10,
				10);
	editKeyButton.get()->connect("pressed", &MainMenu::onEditKeyButtonPressed, this);
	editKeyButton.get()->setText("<Select action>");
	optionsGui->add(editKeyButton, "editKeyButton");
	//onKeySelected();
	// Back to main button
	tgui::Button::Ptr backToMenuButton = tgui::Button::create();
	backToMenuButton.get()->setPosition(10, GAME_HEIGHT - 42);
	backToMenuButton.get()->connect("pressed", &MainMenu::onBackToMenuButtonPressed, this);
	backToMenuButton->setText("Back to menu");
	optionsGui->add(backToMenuButton, "backToMenuButton");
}

MainMenu::~MainMenu()
{
    delete mainGui;
	delete optionsGui;
}

void MainMenu::onEditKeyButtonPressed()
{
	isEditingKey = true;
	tgui::Button::Ptr editKeyButton = optionsGui->get<tgui::Button>("editKeyButton");
	editKeyButton.get()->setText("Press key...");
}

void MainMenu::onKeySelected()
{
	
	tgui::ListBox::Ptr keysListBox = optionsGui->get<tgui::ListBox>("keysListBox");
	tgui::Button::Ptr editKeyButton = optionsGui->get<tgui::Button>("editKeyButton");

	if (!keysListBox.get()) {
		wcout << "keysListBox is null ! Exiting MainMenu::onKeySelected() " << keysListBox.get() << endl;
		return;
	}

	string keyName = keysListBox.get()->getSelectedItem();
	int keyCode = -1;
	if (keyName == "Up") {
		keyCode = Settings::getInstance().gameKeys[eGameKey::Up];
	} else if (keyName == "Left") {
		keyCode = Settings::getInstance().gameKeys[eGameKey::Left];
	} else if (keyName == "Right") {
		keyCode = Settings::getInstance().gameKeys[eGameKey::Right];
	} else if (keyName == "Reload") {
		keyCode = Settings::getInstance().gameKeys[eGameKey::Reload];
	}
	

	string selectedKeyName = VTools::getKeyName((sf::Keyboard::Key)keyCode);
	editKeyButton.get()->setText(keyName + string(" : " + selectedKeyName));
	
}

void MainMenu::onOptionsButtonPressed()
{
	currentGui = optionsGui;
}

void MainMenu::onBackToMenuButtonPressed()
{
	currentGui = mainGui;
}

void MainMenu::onHostButtonPressed()
{
    cout << "Hosting server..." << endl;
    GameRoom* serverRoom = new GameRoom(currentRoom->window);
    serverRoom->backgroundColor = Color(128, 192, 128);
    Server* s = serverRoom->createGameObject<Server>();
    s->isPersistent = true;
    currentRoom->roomToSwitchTo = serverRoom;
}

void MainMenu::onJoinButtonPressed()
{
    cout << "Client..." << endl;
    Client* c = currentRoom->createGameObject<Client>();
    c->isPersistent = true;
    tgui::EditBox::Ptr ipTextBox = mainGui->get<tgui::EditBox>("ipEditBox");
    string ipString = ipTextBox.get()->getText();
    c->connect(ipString, NET_TCPPORT);
}

void MainMenu::onEditorButtonPressed()
{
    cout << "Room editor" << endl;
    GameRoom* newRoomToEdit = new GameRoom(currentRoom->window);
    newRoomToEdit->backgroundColor = Color(192, 192, 192);
    RoomEditor* re = newRoomToEdit->createGameObject<RoomEditor>();
    re->isPersistent = true;
    currentRoom->roomToSwitchTo = newRoomToEdit;
}

void MainMenu::handleEvent(Event e)
{
	currentGui->handleEvent(e);
	if (e.type == Event::KeyPressed)
	{
		// Key editing
		if (isEditingKey)
		{
			int newKey = e.key.code;
			isEditingKey = false;

			tgui::ListBox::Ptr keysListBox = optionsGui->get<tgui::ListBox>("keysListBox");
			tgui::Button::Ptr editKeyButton = optionsGui->get<tgui::Button>("editKeyButton");
			
			if (!keysListBox.get()) {
				wcout << "keysListBox is null ! Exiting MainMenu::handleEvent() " << keysListBox.get() << endl;
				return;
			}

			string keyName = keysListBox.get()->getSelectedItem();
			if (keyName == "Up") {
				Settings::getInstance().gameKeys[eGameKey::Up] = (sf::Keyboard::Key)newKey;
			} else if (keyName == "Left") {
				Settings::getInstance().gameKeys[eGameKey::Left] = (sf::Keyboard::Key)newKey;
			} else if (keyName == "Right") {
				Settings::getInstance().gameKeys[eGameKey::Right] = (sf::Keyboard::Key)newKey;
			} else if (keyName == "Reload") {
				Settings::getInstance().gameKeys[eGameKey::Reload] = (sf::Keyboard::Key)newKey;
			}
			Settings::getInstance().writeSettings();

			onKeySelected();
			
		}
	}
}

void MainMenu::draw(RenderWindow* w)
{
    w->draw(menuText);
	currentGui->draw();
}
