#include "gameroom.h"
//#include <QDebug>
#include "SFML/Audio/Listener.hpp"
#include "block.h"
#include "gameobject.h"
#include "player.h"
#include "roomeditor.h"
#include <iostream>

GameRoom *GameRoom::currentRoom = NULL;

GameRoom::GameRoom(RenderWindow *w) {
  this->window = w;
  // this->world = new b2World(gravity);
  if (GameRoom::currentRoom == NULL) {
    GameRoom::currentRoom = this;
  }
  view.setSize(GAME_WIDTH, GAME_HEIGHT);
  view.setCenter(0, 0);
  view.setViewport(FloatRect(0, 0, 1, 1));
  // soundListener = new Listener;
}

GameRoom::~GameRoom() {
  // Free game objects
  auto itEnd = gameObjects.end();
  for (auto it = gameObjects.begin(); it != itEnd; ++it) {
    GameObject *o = *it;
    if (o != NULL) {
      if (!o->isPersistent)
        delete o;
    }
  }

  // Deallocate pointers
  // delete world;
  // delete soundListener;
}

void GameRoom::handleEvent(Event e) {
  // Route event to all game objects
  auto itEnd = gameObjects.end();
  for (auto it = gameObjects.begin(); it != itEnd; ++it) {
    GameObject *o = *it;
    if (o != NULL) {
      if (o->destroyLater)
        continue;
      o->handleEvent(e);
    }
  }
}

int32_t velocityIterations = 6;
int32_t positionIterations = 2;

void GameRoom::update(sf::Time deltaT) {
  // Simulate world physics
  // world->Step(deltaT.asSeconds(), velocityIterations, positionIterations);

  // Update default view and associated sound listener
  window->setView(view);
  // soundListener->setPosition(Vector3f(view.getCenter().x, 0,
  // view.getCenter().y));

  // Update all game objects
  auto itEnd = gameObjects.end();
  for (auto it = gameObjects.begin(); it != itEnd; ++it) {
    GameObject *o = *it;
    if (o != NULL) {
      if (o->destroyLater)
        continue;
      o->update(deltaT);
    }
  }
}

void GameRoom::draw(RenderWindow *w) {
  // Draw all game objects
  /*RectangleShape bgShape(Vector2f(width, height));
bgShape.setFillColor(backgroundColor);
  w->draw(bgShape);*/
  auto itEnd = gameObjects.end();
  for (auto it = gameObjects.begin(); it != itEnd; ++it) {
    GameObject *o = *it;
    if (o != NULL) {
      if (o->destroyLater)
        continue;
      o->draw(w);
    }
  }
}

void GameRoom::drawUi(RenderWindow *w) {
  // Draw all game object's UI (drawn above normal draw events)
  auto itEnd = gameObjects.end();
  for (auto it = gameObjects.begin(); it != itEnd; ++it) {
    GameObject *o = *it;
    if (o != NULL) {
      if (o->destroyLater)
        continue;
      o->drawUi(w);
    }
  }
}

// Deletes all gameobjects that have their destroyLater flag set to true
size_t GameRoom::purgeObjects() {
  vector<GameObject *> copyGameObjects;
  vector<GameObject *> objectsToDestroy;

  // Keep objects without destroyLater set to true
  for (auto o : gameObjects) {
    if (o != NULL) {
      if (!o->destroyLater) {
        copyGameObjects.push_back(o);
      } else {
        objectsToDestroy.push_back(o);
      }
    }
  }
  gameObjects = copyGameObjects;

  // Deallocate memory of objects to remove
  for (size_t i = 0; i < objectsToDestroy.size(); i++) {
    GameObject *o = objectsToDestroy.at(i);
    if (o != NULL) {
      //            cout << "deleting game object " << o << endl;
      delete o;
    }
    o = NULL;
  }

  return objectsToDestroy.size();
}

size_t GameRoom::addObjects() {
  for (auto o : gameObjectsToAdd) {
    if (o != NULL) {
      gameObjects.push_back(o);
    }
  }
  size_t sz = gameObjectsToAdd.size();
  gameObjectsToAdd.clear();
  return sz;
}

void GameRoom::handleKeyPressed(Event e) {
  switch (e.key.code) {
  case Keyboard::S: {

    break;
  }
  }
}
