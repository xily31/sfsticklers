#ifndef VTOOLS_H
#define VTOOLS_H

#include "SFML/Graphics.hpp"
#include "SFML/System.hpp"
#include <string>
#include "defines.h"

using namespace std;
using namespace sf;

class VTools
{
public:
    static float pointDirection(Vector2f point1, Vector2f point2);
    static float pointDistance(Vector2f point1, Vector2f point2);
    static float degToRad(float angle);
    static float lengthDirX(float direction, float length);
    static float lengthDirY(float direction, float length);
    static float linesIntersect(Vector2f point1, Vector2f point2, Vector2f point3, Vector2f point4, bool isSegment);
    static string iniReadString(string fileName, string section, string key);
    static void iniWriteString(string fileName, string section, string key, const string& value);
    static bool lineRectangleIntersection(float x1, float y1, float x2, float y2, FloatRect, Vector2f*);
	static string getKeyName( const sf::Keyboard::Key key );
    static float roundPrecision(float floatToRound, int precision);
    static float random();
    static vector<string> listDirFiles(string pExt = "", string pDir = "");
	static void drawLine(RenderWindow* w, const Vector2f& pPoint1, const Vector2f& pPoint2, const Color& pColor = Color(0,0,0,255));

    template<typename Out>
    static void split(const std::string &s, char delim, Out result);
    static std::vector<std::string> split(const std::string &s, char delim);

    // Removes all occurences of eltToRemove from a vector. Returns the size difference before/after.
    template<typename T> static size_t removeAll(vector<T>& vec, T eltToRemove)
    {
        size_t initialSize = vec.size();
        vector<T> buffer;
        for (auto i : vec)
        {
            if (i != eltToRemove) {
                buffer.push_back(i);
            }
        }
        vec = buffer;
        return initialSize - vec.size();
    }

    template<typename T> static size_t removeAll(vector<T*>& vec, T* eltToRemove)
    {
        size_t initialSize = vec.size();
        vector<T*> buffer;
        for (auto i : vec)
        {
            if (i != eltToRemove) {
                buffer.push_back(i);
            } else {
                eltToRemove = NULL;
            }
        }
        vec = buffer;
        return initialSize - vec.size();
    }
};

#endif // VTOOLS_H
