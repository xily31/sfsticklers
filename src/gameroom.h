#ifndef GAMEROOM_H
#define GAMEROOM_H

#include "SFML/Graphics.hpp"
#include "SFML/System.hpp"
#include "defines.h"
#include <vector>

using namespace std;

using namespace sf;

class GameObject;
namespace sf {
	class Listener;
}


class GameRoom
{
public:
    GameRoom(RenderWindow*);
    virtual ~GameRoom();
    void handleEvent(Event);
    void update(sf::Time);
    void draw(RenderWindow*);
    void drawUi(RenderWindow*);
    static GameRoom* currentRoom;
    size_t purgeObjects();
    size_t addObjects();
    template<typename T> T* createGameObject()
    {
        T* newObj = new T(this);
        gameObjectsToAdd.push_back(newObj);
        return newObj;
    }
    template<typename T> vector<T*> getAllObjects()
    {
        vector<T*> objects;
        for (auto o : gameObjects)
        {
            if (o != NULL) {
                if (T* gameObj = dynamic_cast<T*>(o))
                    objects.push_back(gameObj);
            }
        }
        return objects;
    }
    template<typename T> void addExistingGameObject(T* o)
    {
        gameObjects.push_back((T*)o);
    }
    void saveMapDialog();
    void loadMapDialog();
    void handleKeyPressed(Event);

    Vector2f boundaries = Vector2f(2048, 2048);
    Color backgroundColor = Color(192, 192, 192, 255);
    vector<GameObject*> gameObjects;
    vector<GameObject*> gameObjectsToAdd;
    RenderWindow* window;
    GameRoom* roomToSwitchTo = NULL;
    float width = 2048;
	float height = 2048;
	View view;
	//sf::Listener* soundListener;

    // Define the gravity vector.
    //b2Vec2 gravity = b2Vec2(0.f, -10.f);

    // Construct a world object, which will hold and simulate the rigid bodies.
    //b2World* world;
private:
    RectangleShape backgroundShape;
};

#endif // GAMEROOM_H
