#ifndef GLOBALS_H
#define GLOBALS_H

// Game settings
#define GAME_WIDTH 800
#define GAME_HEIGHT 450
#define GAME_MAXPLAYERS 32
#define FILE_MAP_EXTENSION ".stm"
#define GAME_RESPAWNTIME 3
#define GAME_HEADSHOTOFFSET 9

// Network packet defines
#define NETID_INITPACKET 0
#define NETID_MAINPACKET 1
#define NETID_TCPQUIT 2
#define NETID_PLAYERJOIN 3
#define NETID_PLAYERQUIT 4
#define NETID_SHOT 5
#define NETID_REQMAP 6
#define NETID_REQMAPCONTENT 7
#define NETID_NEWBLOCK 8
#define NETID_SPAWN 9
#define NETID_DEATH 10
#define NETID_PLAYERHIT 11
#define NETID_HEALTH 12
#define NETID_GRAPHICSTATE 13

#define NETCOM_GORIGHT 0
#define NETCOM_GOLEFT 1
#define NETCOM_JUMP 2
#define NETCOM_STOPGORIGHT 3
#define NETCOM_STOPGOLEFT 4
#define NETCOM_STOPJUMP 5

// Network ports setting
#define NET_TCPPORT 6510
#define NET_UDPCLIENTPORT 6511
#define NET_UDPSERVERPORTSSTART 6520
#define NET_UDPSERVERPORTSRANGE 32
#define NET_UDPCLIENTPORTSRANGE 32

// Box2D
#define PPM 32
enum B2dFamily : unsigned short
{
    eWorldDynamic = 0x0001,
    eWorldStatic = 0x0002
};

// Math
#define M_PI 3.14159265358979323846

// Typedefs
typedef unsigned short ushort;
typedef unsigned int uint;

enum SpriteIndex : int
{
    sStand = 0,
    sRun,
    sRunBack,
    sJump,
    sFall,
    sUsp45,
    sBullet,
    sUsp45Shoot,
    sUsp45Reload,
    sImpact,
    sShell,
    sMuzzleFlash,
    sBloodExp,
    sGibs,
    sBlood,
    sDeath
};

enum SoundIndex : int
{
    sdUsp45Shoot = 0,
    sdDie,
    sdClipOut,
    sdClipIn,
    sdLock,
    sdHitFlesh,
    sdHeadshot
};

enum eGameKey
{
	Up = 0,
	Left,
	Right,
	Reload
};

#endif // GLOBALS_H
