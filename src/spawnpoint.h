#ifndef SPAWNPOINT_H
#define SPAWNPOINT_H

#include "gameobject.h"

using namespace sf;

class Spawnpoint : public GameObject
{
    friend class GameRoom;
public:
    Spawnpoint(GameRoom* r);
    virtual void draw(RenderWindow *);
    CircleShape shape;
	int radius = 16;
};

#endif // SPAWNPOINT_H
