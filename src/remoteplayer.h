#ifndef REMOTEPLAYER_H
#define REMOTEPLAYER_H

#include "gameobject.h"
#include "SFML/Network.hpp"
#include "server.h"
#include "animsprite.h"


class Server;

struct SocketHolder
{
    TcpSocket tcpSocket; // 6510
};

/**
 * @brief The RemotePlayer class : serverside representation of a player.
 * TODO : write collision controls serverside
 */
class RemotePlayer : public GameObject
{
    friend class GameRoom;

public:
    RemotePlayer(GameRoom*, SocketHolder*);
    ~RemotePlayer();
    virtual void update(Time);
    virtual void draw(RenderWindow *);
    void sendTcp(Packet);
    void sendUdp(Packet);
    void sendToOthers(Packet, bool isUdp = false); // Packet to send, isUdp (by default, false)
    void handleTcpPacket(Packet);
    void handleUdpPacket(Packet);
    void handleNetCom(int16_t pNetCom);
    void die();
    void respawn();
    void healthUpdate();
    void applyMovement();

    unsigned short udpReceivePort = 0; // 65xx
    unsigned short udpSendPort = 0; // 65xx
    short mpId = -1;
    bool isDisconnected = false;
    Server* server = NULL;
    std::string remoteIp;
    bool isDead = false;
    float respawnCounter = 0;
    int health = 100;
    SocketHolder* socketHolder;
    TcpSocket* tcpSocket;
    UdpSocket udpSocket; // bind to 65xx, send to 6511

    //b2Body* body;

private:
    //b2Vec2 computeRunReaction(const b2Vec2& checkVector);

    bool isGoingLeft = false;
    bool isGoingRight = false;
    bool isJumping = false;

};

#endif // REMOTEPLAYER_H
