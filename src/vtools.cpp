#include "vtools.h"
#include "configfile.h"
#include <fstream>
#include <iostream>
#include <sstream>

//#include "dirent.h"
#include <filesystem>

#include <cmath>
#ifndef WIN32
#include <sys/types.h>
#endif

using namespace std;
using namespace std::filesystem;

float VTools::pointDirection(Vector2f point1, Vector2f point2) {
  float angleResult;
  Vector2f dirVec = Vector2f(point2.x - point1.x, point2.y - point1.y);
  float magSquare = std::sqrt((dirVec.x * dirVec.x) + (dirVec.y * dirVec.y));
  dirVec.x = (dirVec.x) / magSquare;
  angleResult = std::acos(dirVec.x) * (180 / M_PI);
  if (point2.y > point1.y)
    angleResult *= -1;
  if (angleResult < 0)
    angleResult = 360 + angleResult;
  return angleResult;
}

void VTools::drawLine(RenderWindow *w, const Vector2f &pPoint1,
                      const Vector2f &pPoint2, const Color &pColor) {
  sf::Vertex line[2] = {sf::Vertex(sf::Vector2f(pPoint1.x, pPoint1.y), pColor),
                        sf::Vertex(sf::Vector2f(pPoint2.x, pPoint2.y), pColor)};
  w->draw(line, 2, sf::Lines);
}

float VTools::random() {
  float ret = (rand() / (RAND_MAX + 1.0));
  // cout << "random : " << ret << endl;
  return ret;
}

float VTools::degToRad(float angle) { return angle * M_PI / 180; }

float VTools::lengthDirX(float direction, float length) // Need radian
{
  return length * cos(direction);
}

float VTools::lengthDirY(float direction, float length) // Need radian
{
  return length * sin(direction);
}

float VTools::linesIntersect(Vector2f point1, Vector2f point2, Vector2f point3,
                             Vector2f point4, bool isSegment) {
  float ua, ub, ud, ux, uy, vx, vy, wx, wy;
  ua = 0;
  ux = point2.x - point1.x;
  uy = point2.y - point1.y;
  vx = point4.x - point3.x;
  vy = point4.y - point3.y;
  wx = point1.x - point3.x;
  wy = point1.y - point3.y;
  ud = vy * ux - vx * uy;
  if (ud != 0) {
    ua = (vx * wy - vy * wx) / ud;
    if (isSegment) {
      ub = (ux * wy - uy * wx) / ud;
      if (ua < 0 || ua > 1 || ub < 0 || ub > 1)
        ua = 0;
    }
  }
  return ua;
}

float VTools::pointDistance(Vector2f point1, Vector2f point2) {
  return sqrt(pow(point2.x - point1.x, 2) + pow(point2.y - point1.y, 2));
}

string VTools::iniReadString(string fileName, string section, string key) {
  ConfigFile ini(fileName);
  string ret;
  try {
    ret = ini.Value(section, key);
  } catch (const char *e) {
    cout << "Could not read ini, " << e << endl;
    ret = "";
  }
  return ret;
}

void VTools::iniWriteString(string fileName, string section, string key,
                            const string &value) {
  ConfigFile ini(fileName);
  ini.setValue(section, key, value);
  ini.save();
}

float VTools::roundPrecision(float floatToRound, int precision) {
  floatToRound *= precision;
  floatToRound = round(floatToRound);
  floatToRound /= precision;
  return floatToRound;
}

bool VTools::lineRectangleIntersection(float x1, float y1, float x2, float y2,
                                       FloatRect rect, Vector2f *output) {
  vector<Vector2f> intPoints;
  float rx2 = rect.left + rect.width;
  float ry2 = rect.top + rect.height;
  bool isIntersect = false;

  float intCoef = linesIntersect(Vector2f(x1, y1), Vector2f(x2, y2),
                                 Vector2f(rect.left, rect.top),
                                 Vector2f(rect.left, ry2), true);
  if (intCoef > 0)
    intPoints.push_back(
        Vector2f(x1 + intCoef * (x2 - x1), y1 + intCoef * (y2 - y1)));

  intCoef = linesIntersect(Vector2f(x1, y1), Vector2f(x2, y2),
                           Vector2f(rect.left, ry2), Vector2f(rx2, ry2), true);
  if (intCoef > 0)
    intPoints.push_back(
        Vector2f(x1 + intCoef * (x2 - x1), y1 + intCoef * (y2 - y1)));

  intCoef = linesIntersect(Vector2f(x1, y1), Vector2f(x2, y2),
                           Vector2f(rx2, rect.top), Vector2f(rx2, ry2), true);
  if (intCoef > 0)
    intPoints.push_back(
        Vector2f(x1 + intCoef * (x2 - x1), y1 + intCoef * (y2 - y1)));

  intCoef = linesIntersect(Vector2f(x1, y1), Vector2f(x2, y2),
                           Vector2f(rect.left, rect.top),
                           Vector2f(rx2, rect.top), true);
  if (intCoef > 0)
    intPoints.push_back(
        Vector2f(x1 + intCoef * (x2 - x1), y1 + intCoef * (y2 - y1)));

  float shortestColDist = -1;
  Vector2f closestIntPoint;
  for (auto it = intPoints.begin(); it != intPoints.end(); ++it) {
    Vector2f intPoint = *it;
    float colDist = VTools::pointDistance(Vector2f(x1, y1), intPoint);
    if (colDist < shortestColDist || shortestColDist == -1) {
      shortestColDist = colDist;
      closestIntPoint.x = intPoint.x;
      closestIntPoint.y = intPoint.y;
      isIntersect = true;
    }
  }

  if (isIntersect)
    *output = closestIntPoint;

  return isIntersect;
}

template <typename Out>
void VTools::split(const std::string &s, char delim, Out result) {
  std::stringstream ss;
  ss.str(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    *(result++) = item;
  }
}

std::vector<std::string> VTools::split(const std::string &s, char delim) {
  std::vector<std::string> elems;
  split(s, delim, std::back_inserter(elems));
  return elems;
}

string VTools::getKeyName(const sf::Keyboard::Key key) {
  switch (key) {
  default:
  case sf::Keyboard::Unknown:
    return "Unknown";
  case sf::Keyboard::A:
    return "A";
  case sf::Keyboard::B:
    return "B";
  case sf::Keyboard::C:
    return "C";
  case sf::Keyboard::D:
    return "D";
  case sf::Keyboard::E:
    return "E";
  case sf::Keyboard::F:
    return "F";
  case sf::Keyboard::G:
    return "G";
  case sf::Keyboard::H:
    return "H";
  case sf::Keyboard::I:
    return "I";
  case sf::Keyboard::J:
    return "J";
  case sf::Keyboard::K:
    return "K";
  case sf::Keyboard::L:
    return "L";
  case sf::Keyboard::M:
    return "M";
  case sf::Keyboard::N:
    return "N";
  case sf::Keyboard::O:
    return "O";
  case sf::Keyboard::P:
    return "P";
  case sf::Keyboard::Q:
    return "Q";
  case sf::Keyboard::R:
    return "R";
  case sf::Keyboard::S:
    return "S";
  case sf::Keyboard::T:
    return "T";
  case sf::Keyboard::U:
    return "U";
  case sf::Keyboard::V:
    return "V";
  case sf::Keyboard::W:
    return "W";
  case sf::Keyboard::X:
    return "X";
  case sf::Keyboard::Y:
    return "Y";
  case sf::Keyboard::Z:
    return "Z";
  case sf::Keyboard::Num0:
    return "Num0";
  case sf::Keyboard::Num1:
    return "Num1";
  case sf::Keyboard::Num2:
    return "Num2";
  case sf::Keyboard::Num3:
    return "Num3";
  case sf::Keyboard::Num4:
    return "Num4";
  case sf::Keyboard::Num5:
    return "Num5";
  case sf::Keyboard::Num6:
    return "Num6";
  case sf::Keyboard::Num7:
    return "Num7";
  case sf::Keyboard::Num8:
    return "Num8";
  case sf::Keyboard::Num9:
    return "Num9";
  case sf::Keyboard::Escape:
    return "Escape";
  case sf::Keyboard::LControl:
    return "LControl";
  case sf::Keyboard::LShift:
    return "LShift";
  case sf::Keyboard::LAlt:
    return "LAlt";
  case sf::Keyboard::LSystem:
    return "LSystem";
  case sf::Keyboard::RControl:
    return "RControl";
  case sf::Keyboard::RShift:
    return "RShift";
  case sf::Keyboard::RAlt:
    return "RAlt";
  case sf::Keyboard::RSystem:
    return "RSystem";
  case sf::Keyboard::Menu:
    return "Menu";
  case sf::Keyboard::LBracket:
    return "LBracket";
  case sf::Keyboard::RBracket:
    return "RBracket";
  case sf::Keyboard::SemiColon:
    return "SemiColon";
  case sf::Keyboard::Comma:
    return "Comma";
  case sf::Keyboard::Period:
    return "Period";
  case sf::Keyboard::Quote:
    return "Quote";
  case sf::Keyboard::Slash:
    return "Slash";
  case sf::Keyboard::BackSlash:
    return "BackSlash";
  case sf::Keyboard::Tilde:
    return "Tilde";
  case sf::Keyboard::Equal:
    return "Equal";
  case sf::Keyboard::Dash:
    return "Dash";
  case sf::Keyboard::Space:
    return "Space";
  case sf::Keyboard::Return:
    return "Return";
  case sf::Keyboard::BackSpace:
    return "BackSpace";
  case sf::Keyboard::Tab:
    return "Tab";
  case sf::Keyboard::PageUp:
    return "PageUp";
  case sf::Keyboard::PageDown:
    return "PageDown";
  case sf::Keyboard::End:
    return "End";
  case sf::Keyboard::Home:
    return "Home";
  case sf::Keyboard::Insert:
    return "Insert";
  case sf::Keyboard::Delete:
    return "Delete";
  case sf::Keyboard::Add:
    return "Add";
  case sf::Keyboard::Subtract:
    return "Subtract";
  case sf::Keyboard::Multiply:
    return "Multiply";
  case sf::Keyboard::Divide:
    return "Divide";
  case sf::Keyboard::Left:
    return "Left";
  case sf::Keyboard::Right:
    return "Right";
  case sf::Keyboard::Up:
    return "Up";
  case sf::Keyboard::Down:
    return "Down";
  case sf::Keyboard::Numpad0:
    return "Numpad0";
  case sf::Keyboard::Numpad1:
    return "Numpad1";
  case sf::Keyboard::Numpad2:
    return "Numpad2";
  case sf::Keyboard::Numpad3:
    return "Numpad3";
  case sf::Keyboard::Numpad4:
    return "Numpad4";
  case sf::Keyboard::Numpad5:
    return "Numpad5";
  case sf::Keyboard::Numpad6:
    return "Numpad6";
  case sf::Keyboard::Numpad7:
    return "Numpad7";
  case sf::Keyboard::Numpad8:
    return "Numpad8";
  case sf::Keyboard::Numpad9:
    return "Numpad9";
  case sf::Keyboard::F1:
    return "F1";
  case sf::Keyboard::F2:
    return "F2";
  case sf::Keyboard::F3:
    return "F3";
  case sf::Keyboard::F4:
    return "F4";
  case sf::Keyboard::F5:
    return "F5";
  case sf::Keyboard::F6:
    return "F6";
  case sf::Keyboard::F7:
    return "F7";
  case sf::Keyboard::F8:
    return "F8";
  case sf::Keyboard::F9:
    return "F9";
  case sf::Keyboard::F10:
    return "F10";
  case sf::Keyboard::F11:
    return "F11";
  case sf::Keyboard::F12:
    return "F12";
  case sf::Keyboard::F13:
    return "F13";
  case sf::Keyboard::F14:
    return "F14";
  case sf::Keyboard::F15:
    return "F15";
  case sf::Keyboard::Pause:
    return "Pause";
  }
}

vector<string> VTools::listDirFiles(string pExt, string pDir) {
	vector<string> filesList;
	if (pDir.size() <= 0) pDir = ".";
	try {
		for (auto& p : directory_iterator(pDir))
		{
			if (pExt.size() <= 0) {
				filesList.push_back(path(p).string());
			}
			else if (path(p).extension() == "." + pExt) {
				filesList.push_back(path(p).string());
			}
		}
	}
	catch (exception e)
	{
		wcout << "VTools::listDirFiles exception ! " << e.what() << ", directory : " << pDir << endl;
	}
	return filesList;
}
