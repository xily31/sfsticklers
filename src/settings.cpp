#include "settings.h"
#include "vtools.h"
#include <iostream>

Settings* Settings::mInstance = 0;

Settings::Settings()
{
	readSettings();
}

Settings& Settings::getInstance()
{
	if (!mInstance)
	{
		mInstance = new Settings;
	}
	return *mInstance;
}

bool Settings::readSettings()
{
	serverIp = VTools::iniReadString("settings.ini", "NETWORK", "ServerIp");
	gameKeys[eGameKey::Up] = readKey("Up");
	gameKeys[eGameKey::Left] = readKey("Left");
	gameKeys[eGameKey::Right] = readKey("Right");
	gameKeys[eGameKey::Reload] = readKey("Reload");
	return true;
}

void Settings::writeSettings()
{
	VTools::iniWriteString("settings.ini", "NETWORK", "ServerIp", serverIp);

	VTools::iniWriteString("settings.ini", "KEYCONFIG", "Up", std::to_string(gameKeys[eGameKey::Up]));
	VTools::iniWriteString("settings.ini", "KEYCONFIG", "Left", std::to_string(gameKeys[eGameKey::Left]));
	VTools::iniWriteString("settings.ini", "KEYCONFIG", "Right", std::to_string(gameKeys[eGameKey::Right]));
	VTools::iniWriteString("settings.ini", "KEYCONFIG", "Reload", std::to_string(gameKeys[eGameKey::Reload]));
}

sf::Keyboard::Key Settings::readKey(std::string keyName)
{
	sf::Keyboard::Key retKey;
	try {
		retKey = (sf::Keyboard::Key) std::stoi(VTools::iniReadString("settings.ini", "KEYCONFIG", keyName));
	} catch (invalid_argument e) {
		std::cout << "Settings::readKey Could not convert key from settings.ini" << std::endl;
		retKey = sf::Keyboard::A;
	}
	return retKey;
}
