#include "spawnpoint.h"

Spawnpoint::Spawnpoint(GameRoom* r)
{
    this->currentRoom = r;
	shape.setFillColor(Color::Blue);
	shape.setRadius(radius);
	shape.setOrigin(radius, radius);
}

void Spawnpoint::draw(RenderWindow* w)
{
    shape.setPosition(position);
    w->draw(shape);
}
