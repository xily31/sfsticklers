#include "bulletimpact.h"

BulletImpact::BulletImpact(GameRoom* r)
{
    this->currentRoom = r;
}

void BulletImpact::update(Time t)
{
    if (alpha <= 0) destroyLater = true;
    alpha -= t.asSeconds() * 1;
}

void BulletImpact::draw(RenderWindow * w)
{
    CircleShape c;
    c.setRadius(8);
    c.setFillColor(Color(255, 0, 0, alpha*255));
    c.setPosition(position);
    c.setOrigin(8, 8);
    w->draw(c);
}
