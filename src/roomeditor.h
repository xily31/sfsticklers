#ifndef ROOMEDITOR_H
#define ROOMEDITOR_H

#include "gameobject.h"
#include "promptmanager.h"
#include "player.h"

namespace tgui
{
    class Gui;
}

class RoomEditor : public GameObject
{
    friend class GameRoom;
    enum EditMode {
        eRectangleMode = 0,
        ePolygonMode,
        eSpawnpointMode
    };

public:
    virtual ~RoomEditor();

    EditMode editMode = eRectangleMode;
	int gridSize = 16;

protected:
    RoomEditor(GameRoom*);
    virtual void handleEvent(Event);
    virtual void update(Time);
	virtual void draw(RenderWindow *);
    virtual void drawUi(RenderWindow *);
    void handleKeyPressed(Event);
    void handleKeyReleased(Event);
    void handleTextEntered(Event);
    void handleMouse(Event);
    void saveMap(string);
    Vector2f getMousePos() { return currentRoom->window->mapPixelToCoords(Mouse::getPosition(*(currentRoom->window))); }
    void onEditModeButtonPressed(EditMode newMode);

private:
    float lastX = 0, lastY = 0;
    bool isDrawing = false;
    bool isOnline = false;
	//View view;
    bool isDraggingView = false;
    Vector2f mouseDragOrigin;
    Text currentText;
    Font arialFont;
    PromptManager prompter;
    enum PromptType {
        SaveMap,
        LoadMap
    };
    PromptType promptType = SaveMap;
    Player* testPlayer = NULL;
    vector<Vector2f> polygonBlockEdited;
    tgui::Gui* gui;
	Vector2i cursorPos; // Mouse pos. modulo gridsize
	float zoomFactor = 1;
};

#endif // ROOMEDITOR_H
