#include "bullet.h"
#include "block.h"
#include "vtools.h"
#include "resloader.h"
#include "otherplayer.h"
#include "bulletimpact.h"
#include "player.h"
#include <iostream>
#include "visualfx.h"
#include <cmath>

Bullet::Bullet(GameRoom* r)
{
    this->currentRoom = r;
    sprites[sBullet] = ResLoader::getInstance()->getSprite(sBullet);
    spriteIndex = sBullet;
    sprites[spriteIndex].origin = Vector2f(24, 24);
    vector<Player*> playerObjs = currentRoom->getAllObjects<Player>();
    playerObj = NULL;
    if (playerObjs.size() > 0) {
        if (playerObjs.at(0)) {
            playerObj = playerObjs.at(0);
        }
    }
}

Bullet::~Bullet()
{
	
}

void Bullet::update(Time t)
{
    vector<Block*> blocks = currentRoom->getAllObjects<Block>();
    Vector2f potPos;
    float radDir = VTools::degToRad(direction);
    potPos.x = position.x + VTools::lengthDirX(radDir, velocity) * t.asMicroseconds()/10000;
    potPos.y = position.y - VTools::lengthDirY(radDir, velocity) * t.asMicroseconds()/10000;
    float shortestColDist = -1;
    float closestColX = -1;
    float closestColY = -1;

    // Block hit by bullet
    for (auto b : blocks)
    {
        // TODO : check if already in a block
//        FloatRect blockRect = Rect<float>(b->getPosition().x, b->getPosition().y, b->getBounds().width, b->getBounds().height);

        vector<pair<Vector2f, Vector2f>> linesToCheck;
        if (cos(VTools::degToRad(direction)) > 0) { // Collisions when going right
            linesToCheck.push_back(pair<Vector2f, Vector2f>(b->getPosition(),
                                                          Vector2f(b->getPosition().x, b->getPosition().y+b->getBounds().height)));
        }
        if (sin(VTools::degToRad(direction)) > 0) { // Collisions when going up
            linesToCheck.push_back(pair<Vector2f, Vector2f>(Vector2f(b->getPosition().x, b->getPosition().y+b->getBounds().height),
                                                          Vector2f(b->getPosition().x+b->getBounds().width, b->getPosition().y+b->getBounds().height)));
        }
        if (cos(VTools::degToRad(direction)) < 0) { // Collisions when going left
            linesToCheck.push_back(pair<Vector2f, Vector2f>(Vector2f(b->getPosition().x+b->getBounds().width, b->getPosition().y),
                                                          Vector2f(b->getPosition().x+b->getBounds().width, b->getPosition().y+b->getBounds().height)));
        }
        if (sin(VTools::degToRad(direction)) < 0) { // Collisions when going down
            linesToCheck.push_back(pair<Vector2f, Vector2f>(Vector2f(b->getPosition().x, b->getPosition().y),
                                                          Vector2f(b->getPosition().x+b->getBounds().width, b->getPosition().y)));
        }

        for (size_t i=0; i<linesToCheck.size(); i++)
        {
            pair<Vector2f, Vector2f> line = linesToCheck.at(i);
            float intCoef = VTools::linesIntersect(position,
                                                 potPos,
                                                 line.first,
                                                 line.second,
                                                 true);
            if (intCoef > 0)
            {
                float colX = position.x + intCoef * (potPos.x - position.x);
                float colY = position.y + intCoef * (potPos.y - position.y);
                float colDist = VTools::pointDistance(position, Vector2f(colX, colY));
                if (colDist < shortestColDist || shortestColDist == -1)
                {
                    shortestColDist = colDist;
                    closestColX = colX;
                    closestColY = colY;
                }
            }
        }
    }

    bool isFleshHit = false; // Whether to play impact or bloodexp animation on landing

    // Other player hit by bullet
    pair<float, short> victimMpId(-1, -1); // <DistanceToImpact, VictimMpId>
    vector<OtherPlayer*> otherPlayers = currentRoom->getAllObjects<OtherPlayer>();
    for (auto it=otherPlayers.begin(); it!=otherPlayers.end(); ++it)
    {
        OtherPlayer* o = *it;
        if (!o) {
            continue; // Abort if OtherPlayer pointer is null or points to nothing
        }
        if (shooterMpId == o->mpId || o->getIsDead()) {
            continue;
        }
        FloatRect bounds;
        bounds.left = o->getPosition().x + o->hitboxXOffset;
        bounds.top = o->getPosition().y + o->hitboxYOffset;
        bounds.width = o->getHitbox().getSize().x;
        bounds.height = o->getHitbox().getSize().y;
        Vector2f colPoint;
        if (!VTools::lineRectangleIntersection(position.x, position.y, potPos.x, potPos.y, bounds, &colPoint)) continue;
        float colDist = VTools::pointDistance(position, colPoint);
        if (colDist < shortestColDist || shortestColDist == -1)
        {
            shortestColDist = colDist;
            closestColX = colPoint.x;
            closestColY = colPoint.y;
            isFleshHit = true;
            if (playerObj != NULL) { // Get other player mp id if local player hit other player
                if (shooterMpId == playerObj->client->mpId) {
                    victimMpId.first = colDist;
                    victimMpId.second = o->mpId;
                }
            }
        }
    }

    // Local player hit by bullet
    if (playerObj != NULL)
    {
        if (playerObj->client) {
            if (shooterMpId != playerObj->client->mpId && !playerObj->isDead)
            {
                FloatRect bounds;
                bounds.left = playerObj->getPosition().x + playerObj->hitboxXOffset;
                bounds.top = playerObj->getPosition().y + playerObj->hitboxYOffset;
                bounds.width = playerObj->getHitbox().getSize().x;
                bounds.height = playerObj->getHitbox().getSize().y;
                Vector2f colPoint;
                if (VTools::lineRectangleIntersection(position.x, position.y, potPos.x, potPos.y, bounds, &colPoint))
                {
                    float colDist = VTools::pointDistance(position, colPoint);
                    if (colDist < shortestColDist || shortestColDist == -1)
                    {
                        shortestColDist = colDist;
                        closestColX = colPoint.x;
                        closestColY = colPoint.y;
                        isFleshHit = true;
                    }
                }
            }
        }
    }

    // Collision detected
    if (shortestColDist != -1)
    {
        setMotion(direction, 0);
        potPos.x = closestColX;
        potPos.y = closestColY;

        VisualFX* fx = currentRoom->createGameObject<VisualFX>();
        fx->setPosition(potPos);
        fx->init((isFleshHit? sBloodExp : sImpact), sprites[spriteIndex].angle);

        destroyLater = true;
        if (victimMpId.first == shortestColDist) {
            cout << "OTHER PLAYER HIT BY LOCAL PLAYER, OTHER ID :" << victimMpId.second << endl;
            if (playerObj != NULL) { // Tell server we've hit another player
                if (playerObj->client != NULL) {
                    OtherPlayer* o = playerObj->client->getOtherPlayerByMpId(victimMpId.second);
                    if (o) {
                        short sendId = NETID_PLAYERHIT;
                        short sendVictimMpId = victimMpId.second;
                        // TODO : compute this earlier, for both local player and other player
                        float sendHitYOffset = potPos.y - (o->getPosition().y + o->hitboxYOffset);
                        Packet sendPacket;
                        sendPacket << sendId << sendVictimMpId << sendHitYOffset;
                        playerObj->client->sendTcp(sendPacket);
                    }
                }
            }
        }
    }

    // Apply go to computed potential position
    position = potPos;

    // Die after lifeSpawn time is elapsed
    if (lifeCounter >= (lifeSpan-1)) {
        destroyLater = true;
    } else {
        lifeCounter += t.asSeconds();
    }
}

void Bullet::setMotion(float direction, float velocity)
{
    this->direction = direction;
    this->velocity = velocity;
    sprites[spriteIndex].angle = -direction;
}
