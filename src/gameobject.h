#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "gameroom.h"
#include "SFML/Graphics.hpp"
#include "SFML/System.hpp"
#include "animsprite.h"
#include "resloader.h"
//#include <QMap>
#include "defines.h"
#include <string>
#include <map>

using namespace std;

using namespace sf;

class GameRoom;

class GameObject
{
    friend class GameRoom;

public:
    void setSpriteFromTextureFile(string fileName);
    virtual void handleEvent(Event);
    virtual void update(Time);
    virtual void draw(RenderWindow*);
    virtual void drawUi(RenderWindow*);
    virtual void setPosition(Vector2f);
    virtual void setHitbox(Vector2f);
    virtual Vector2f getPosition() { return position; }
    virtual void transferToRoom(GameRoom*);
    map<SpriteIndex, AnimSprite> sprites;
    void setSpriteIndex(SpriteIndex s) { if (spriteIndex != s) spriteIndex = s; }
    SpriteIndex getSpriteIndex() { return spriteIndex; }
    RectangleShape getHitbox() { return hitbox; }
    Vector2f getMousePos();

    Texture texture;
    bool destroyLater = false;
	bool isPersistent = false;

protected:
    GameObject() {}
    virtual ~GameObject();
    GameObject(GameRoom*);

    GameRoom* currentRoom;
    Vector2f position = Vector2f(0, 0);
    RectangleShape hitbox = RectangleShape(Vector2f(32, 32));
    SpriteIndex spriteIndex;
    bool drawDebug = false;
};

#endif // GAMEOBJECT_H
