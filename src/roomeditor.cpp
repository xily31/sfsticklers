#include "roomeditor.h"
#include "block.h"
#include "vtools.h"
#include <iostream>
#include "spawnpoint.h"
#include <fstream>
#include "particle.h"
#include "TGUI/TGUI.hpp"
#include <cmath>

RoomEditor::RoomEditor(GameRoom* r)
{
	this->currentRoom = r;
    arialFont.loadFromFile("res/fonts/arial.ttf");
	currentRoom->window->setView(currentRoom->view);
    currentText.setPosition(10, 10);
    currentText.setFont(arialFont);
    currentText.setColor(Color::Black);

	gui = new tgui::Gui(*r->window);
	currentRoom->view.setCenter(GAME_WIDTH/2, GAME_HEIGHT/2);
	gui->setView(currentRoom->view);

    tgui::Button::Ptr rectangleButton = tgui::Button::create();
    rectangleButton.get()->setPosition(10, 10);
    rectangleButton.get()->connect("pressed", &RoomEditor::onEditModeButtonPressed, this, eRectangleMode);
    rectangleButton->setText("Rectangle");
    gui->add(rectangleButton, "rectangleButton");

    tgui::Button::Ptr polygonButton = tgui::Button::create();
    polygonButton.get()->setPosition(10, 50);
    polygonButton.get()->connect("pressed", &RoomEditor::onEditModeButtonPressed, this, ePolygonMode);
    polygonButton->setText("Polygon");
    gui->add(polygonButton, "polygonButton");

    tgui::Button::Ptr spawnButton = tgui::Button::create();
    spawnButton.get()->setPosition(10, 90);
    spawnButton.get()->connect("pressed", &RoomEditor::onEditModeButtonPressed, this, eSpawnpointMode);
    spawnButton->setText("Spawnpoint");
    gui->add(spawnButton, "spawnButton");

	currentRoom->view.setCenter(0, 0);
	currentRoom->window->setView(currentRoom->view);
}

RoomEditor::~RoomEditor()
{
    delete gui;
}

void RoomEditor::onEditModeButtonPressed(EditMode newMode)
{
    isDrawing = false;
    polygonBlockEdited.clear();
    this->editMode = newMode;
}

void RoomEditor::handleEvent(Event e)
{
    if (gui->handleEvent(e))
    {
        return; // If GUI has used the event, stop its propagation
    }
    if (e.type == Event::TextEntered)
    {
        handleTextEntered(e);
    }
    if (e.type == Event::KeyPressed)
    {
        handleKeyPressed(e);
    }
	if (e.type == Event::MouseButtonPressed || e.type == Event::MouseButtonReleased
			|| e.type == Event::MouseWheelScrolled)
    {
        handleMouse(e);
    }
}

// Mouse events : draw blocks, drag view
void RoomEditor::handleMouse(Event e)
{
    if (testPlayer != NULL)
    {
        return;
    }

    if (e.type == Event::MouseButtonPressed) // MB pressed
    {
        if (e.mouseButton.button == Mouse::Left && !isDrawing) // LMB pressed : start drawing a block
        {
			if (editMode == eRectangleMode) // Rectangle mode
			{
				lastX = cursorPos.x;
				lastY = cursorPos.y;
                isDrawing = true;
            }
			else if (editMode == ePolygonMode) // Polygon mode
			{
                size_t nVertices = polygonBlockEdited.size();
                size_t pos = 0;
                if (nVertices > 0) {
                    pos = nVertices - 1;
                }
				Vector2f mousePos(cursorPos.x, cursorPos.y);
                if (polygonBlockEdited.size() > 0)
                {
					if (polygonBlockEdited.at(polygonBlockEdited.size()-1) == mousePos) // Mouse clicked at last vertex
                    {
                        if (polygonBlockEdited.size() >= 3) { // Save polygon id >=3 vertices
                            Block* b = currentRoom->createGameObject<Block>();
                            vector<sf::Vector2f> vertices;
                            for (uint i=0; i<polygonBlockEdited.size(); ++i) {
                                vertices.push_back(polygonBlockEdited.at(i));
                            }
                            b->initialize(vertices);
                        }
                        polygonBlockEdited.clear(); // Clear polygon creation dummy
                        return;
                    }
                }
				polygonBlockEdited.push_back(mousePos); // Add polygon vertex
            }
			else if (editMode == eSpawnpointMode) // Spawnpoint mode mode
			{
                Spawnpoint* s = currentRoom->createGameObject<Spawnpoint>();
				s->setPosition(getMousePos());
            }
        }
		else if (e.mouseButton.button == Mouse::Middle && !isDraggingView) // MMB pressed : start dragging view
        {
            isDraggingView = true;
            mouseDragOrigin = Vector2f(e.mouseButton.x, e.mouseButton.y);
        }
		else if (e.mouseButton.button == Mouse::Right) // RMB pressed : delete object under cursor
		{
			const vector<Block*>& blocks = currentRoom->getAllObjects<Block>();
			Vector2f mousePos = getMousePos();
			bool removed = false;
			for (int i=blocks.size()-1; i>=0 && !removed; --i)
			{
				Block* block = blocks.at(i);
				/*b2Fixture *f = block->body->GetFixtureList();
				while(f)
				{
					if (f->TestPoint(b2Vec2(mousePos.x/PPM, mousePos.y/-PPM)))
					{
						block->destroyLater = true;
						removed = true;
						break;
					}
					f = f->GetNext();
				}*/
			}

			if (!removed)
			{
				const vector<Spawnpoint*>& spawns = currentRoom->getAllObjects<Spawnpoint>();
				for (int i=spawns.size()-1; i>=0 && !removed; --i)
				{
					Spawnpoint* spawn = spawns.at(i);
					if (VTools::pointDistance(spawn->getPosition(), mousePos) < 16)
					{
						spawn->destroyLater = true;
						removed = true;
						break;
					}
				}
			}
		}
    }
    if (e.type == Event::MouseButtonReleased) // MB released
    {
        if (e.mouseButton.button == Mouse::Left && isDrawing) // LMB released : stop dragging and draws a block
        {
            if (editMode == eRectangleMode) {
                isDrawing = false;
				Vector2f mouseCoords(cursorPos.x, cursorPos.y);
                float mouseX = mouseCoords.x;
                float mouseY = mouseCoords.y;
                float width = abs(mouseX - lastX);
                float height = abs(mouseY - lastY);
                if (width < 16 || height < 16) return; // Abort if block isn't big enough
                Block* b = currentRoom->createGameObject<Block>();
                b->setHitbox(Vector2f(width, height));
                float posX = (mouseX > lastX ? lastX :  mouseX);
                float posY = (mouseY > lastY ? lastY : mouseY);
                b->setPosition(Vector2f(posX, posY));
                vector<sf::Vector2f> blockVertices;
                blockVertices.push_back(Vector2f(posX, posY));
                blockVertices.push_back(Vector2f(posX + width, posY));
                blockVertices.push_back(Vector2f(posX + width, posY + height));
                blockVertices.push_back(Vector2f(posX, posY + height));
                b->initialize(blockVertices);
            } else if (editMode == ePolygonMode) {

            }
        }
        if (e.mouseButton.button == Mouse::Middle) // MMB released : stop dragging view
        {
            isDraggingView = false;
        }
    }
	if (e.type == Event::MouseWheelScrolled) // Mouse scrolled
	{
		currentRoom->view.zoom(1 - e.mouseWheelScroll.delta/10);
		zoomFactor = currentRoom->view.getSize().x / GAME_WIDTH;
		currentRoom->window->setView(currentRoom->view);
	}
}

void RoomEditor::handleTextEntered(Event e)
{
    if (testPlayer != NULL)
    {
        return;
    }

    char newChar = static_cast<char>(e.text.unicode);
    if (newChar == 's' && !prompter.getIsTyping())
    {
        prompter.prompt("Enter save map file name");
        promptType = SaveMap;
    }
    else if (newChar == 'l')
    {
        prompter.prompt("Enter map file name to load (w/out extension)");
        promptType = LoadMap;
    }
    else if (newChar == '\r')
    {
        if (prompter.getIsTyping())
        {
            switch (promptType)
            {
            case SaveMap:
            {
                saveMap(prompter.getResult());
                break;
            }

            case LoadMap:
            {
                string mapFileName = prompter.getResult();
                mapFileName += FILE_MAP_EXTENSION;
                GameRoom* r = ResLoader::loadMap(mapFileName, currentRoom->window);
                if (r == NULL) {
                    cout << "Loaded map is null" << mapFileName << endl;
                    break;
                }
                r->addExistingGameObject<RoomEditor>(this);
                currentRoom->roomToSwitchTo = r;
                break;
            }
            }
        }
    }
    else
    {
        prompter.handleEvent(e);
    }
}

void RoomEditor::handleKeyPressed(Event e)
{
    switch (e.key.code)
    {
    case Keyboard::C:
        cout << "number of walls :" << currentRoom->getAllObjects<Block>().size() << endl;
        break;

    case Keyboard::Num1: // Spawnpoint
    {
        Spawnpoint* s = currentRoom->createGameObject<Spawnpoint>();
        s->setPosition(getMousePos() - Vector2f(s->shape.getRadius(), s->shape.getRadius()));
        break;
    }

    case Keyboard::Num2: // Toggle test player
    {
        if (testPlayer == NULL) {
            testPlayer = currentRoom->createGameObject<Player>();
            testPlayer->setPosition(getMousePos());
        } else {
            testPlayer->destroyLater = true;
            testPlayer = NULL;
			currentRoom->window->setView(currentRoom->view);
        }
        break;
    }

    case Keyboard::T:
    {
        size_t nVertices = polygonBlockEdited.size();
        size_t pos = 0;
        if (nVertices > 0) {
            pos = nVertices - 1;
        }
        polygonBlockEdited.push_back(
                        Vector2f(getMousePos())
                    );
        break;
    }

    case Keyboard::F:
    {
        Block* b = currentRoom->createGameObject<Block>();
        vector<sf::Vector2f> vertices;
        for (uint i=0; i<polygonBlockEdited.size(); ++i) {
            vertices.push_back(polygonBlockEdited.at(i));
        }
        b->initialize(vertices);
        polygonBlockEdited.clear();
        break;
    }

    case Keyboard::B: // Test gibs
    {
        for (int i=0; i<20; ++i)
        {
            cout << "creating particle" << endl;
            Particle* p = currentRoom->createGameObject<Particle>();
            p->init(sGibs);
            p->setMotion(5, 7, 70, 110);
        }
    }

    default:
        break;
    }

}

void RoomEditor::handleKeyReleased(Event e)
{

}

void RoomEditor::update(Time t)
{
	// Compute cursor position
	Vector2f mousePos = getMousePos();
	int signMouseX = mousePos.x/abs(mousePos.x);
	int signMouseY = mousePos.y/abs(mousePos.y);
	cursorPos.x = mousePos.x + gridSize/2 * signMouseX;
	cursorPos.y = mousePos.y + gridSize/2 * signMouseY;
	cursorPos.x -= cursorPos.x % gridSize;
	cursorPos.y -= cursorPos.y % gridSize;

	// Drag view
    if (isDraggingView && !testPlayer)
    {
        Vector2f mousePos = (Vector2f)Mouse::getPosition(*(currentRoom->window));
        if (mousePos == mouseDragOrigin) return;
        float dragDistance = VTools::pointDistance(mouseDragOrigin, mousePos);
        float dragDirection = VTools::pointDirection(mouseDragOrigin, mousePos);
        float dragDeltaX = VTools::lengthDirX(VTools::degToRad(dragDirection), dragDistance);
        float dragDeltaY = VTools::lengthDirY(VTools::degToRad(dragDirection), dragDistance);
        float speedCoef = 2;
		currentRoom->view.move(dragDeltaX*t.asSeconds()*speedCoef, -dragDeltaY*t.asSeconds()*speedCoef);
		currentRoom->window->setView(currentRoom->view);
    }
}

void RoomEditor::draw(RenderWindow * w)
{
	// Draw grid
	if (!testPlayer)
	{
		int startX = currentRoom->view.getCenter().x - currentRoom->view.getSize().x/2;
		startX -= startX % gridSize;
		int startY = currentRoom->view.getCenter().y - currentRoom->view.getSize().y/2;
		startY -= startY % gridSize;
		Color lineColor(0, 0, 0, 32);
		for (int i = 0; i < currentRoom->view.getSize().x; i += gridSize)
		{
			VTools::drawLine(
						w,
						Vector2f(startX + i, startY + 0),
						Vector2f(startX + i, startY + GAME_HEIGHT * zoomFactor),
						lineColor);
		}
		for (int i = 0; i < currentRoom->view.getSize().y; i += gridSize)
		{
			VTools::drawLine(
						w,
						Vector2f(startX + 0, startY + i),
						Vector2f(startX + GAME_WIDTH * zoomFactor, startY + i),
						lineColor
						);
		}
	}
}

void RoomEditor::drawUi(RenderWindow * w)
{
	if (!testPlayer)
	{
		// Draw origin as red circle
		sf::CircleShape circle(4);
		circle.setOrigin(4, 4);
		circle.setPosition(Vector2f(0, 0));
		circle.setOutlineColor(Color::Red);
		circle.setOutlineThickness(1);
		circle.setFillColor(Color::Transparent);
		w->draw(circle);

		// Draw polygon dummy
		if (editMode == ePolygonMode)
		{
			if (polygonBlockEdited.size() > 0 && polygonBlockEdited.size() < 2)
			{
				for (int i = polygonBlockEdited.size()-1; i >= 0; --i)
				{
					if (i == polygonBlockEdited.size()-1) {
						VTools::drawLine(w, polygonBlockEdited.at(i), Vector2f(cursorPos.x, cursorPos.y));
					} else {
						VTools::drawLine(w, polygonBlockEdited.at(i+1), polygonBlockEdited.at(i));
					}
				}
			}
			else if (polygonBlockEdited.size() > 1)
			{
				ConvexShape dummyPolygon(polygonBlockEdited.size()+1);
				for (int i=0; i<polygonBlockEdited.size(); ++i) {
					dummyPolygon.setPoint(i, polygonBlockEdited.at(i));
				}
				Vector2f mousePos(cursorPos.x, cursorPos.y);
				dummyPolygon.setPoint(polygonBlockEdited.size(), mousePos);
				if (mousePos == polygonBlockEdited.at(polygonBlockEdited.size()-1)) {
					dummyPolygon.setFillColor(Color(0, 255, 0, 128));
				} else {
					dummyPolygon.setFillColor(Color(0, 255, 255, 128));
				}
				dummyPolygon.setOutlineColor(Color(0, 0, 0, 128));
				dummyPolygon.setOutlineThickness(1);
				w->draw(dummyPolygon);
			}
		}
		else if (editMode == eRectangleMode && isDrawing)
		{
			float mouseX = cursorPos.x;
			float mouseY = cursorPos.y;
			float width = abs(mouseX - lastX);
			float height = abs(mouseY - lastY);
			float posX = (mouseX > lastX ? lastX :  mouseX);
			float posY = (mouseY > lastY ? lastY : mouseY);
			RectangleShape rectDummy(Vector2f(width, height));
			rectDummy.setPosition(posX, posY);
			rectDummy.setFillColor(Color(0, 255, 0, 128));
			rectDummy.setOutlineColor(Color(0, 0, 0, 128));
			rectDummy.setOutlineThickness(1);
			w->draw(rectDummy);
		}

		// Draw cursor
		VTools::drawLine(
					w,
					Vector2f(cursorPos.x - gridSize, cursorPos.y),
					Vector2f(cursorPos.x + gridSize, cursorPos.y),
					Color::Red);
		VTools::drawLine(
					w,
					Vector2f(cursorPos.x, cursorPos.y - gridSize),
					Vector2f(cursorPos.x , cursorPos.y + gridSize),
					Color::Red);


		// Draw prompter
		if (prompter.getIsTyping())
		{
			prompter.draw(w);
		}

		// Draw UI
		gui->draw();
	}
}

// Save map to file (.stm)
void RoomEditor::saveMap(string fileName)
{
    if (fileName.length() < 3) {
        cout << "File name must be >= 3)" << endl;
        return;
    }
    fileName += FILE_MAP_EXTENSION;
    string dirName = "./";
//    QFile mapFile(dirName + fileName);
    ofstream mapFile;
    mapFile.open(fileName, ios_base::out | ios_base::in);
    if (mapFile.is_open())
    {
        cout << "Overwriting map file" + dirName + fileName << endl;
    }
    else
    {
        mapFile.close();
        mapFile.open(fileName, ios_base::out);
    }
    if ( mapFile.is_open() )
    {
        // Save map size
        mapFile << currentRoom->width << endl;
        mapFile << currentRoom->height << endl;

        // Save blocks
        vector<Block*> blocks = currentRoom->getAllObjects<Block>();
        for (auto it=blocks.begin(); it!=blocks.end(); ++it)
        {
            Block* b = *it;
            mapFile << "block" << endl;
            /*b2PolygonShape* b2PolyShape = (b2PolygonShape*)b->body->GetFixtureList()[0].GetShape();
            for (int i=0; i<b2PolyShape->m_count; i++)
            {
                b2Vec2 point = b2PolyShape->m_vertices[i];
                b2Vec2 absPoint = b->body->GetWorldPoint(point);
                mapFile << absPoint.x << endl;
                mapFile << absPoint.y << endl;
            }*/
            mapFile << "endblock" << endl;
        }
        // Save spawnpoints
        vector<Spawnpoint*> spawns = currentRoom->getAllObjects<Spawnpoint>();
        for (auto it=spawns.begin(); it!=spawns.end(); ++it)
        {
            Spawnpoint* s = *it;
            mapFile << "spawn" << endl;
            mapFile << s->getPosition().x << endl;
            mapFile << s->getPosition().y << endl;
        }
    }
    else
    {
        cout << "Could not open file " << fileName << " for writing" << endl;
    }

}
