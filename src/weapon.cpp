#include "weapon.h"
#include "vtools.h"
#include "resloader.h"
#include <iostream>
#include "visualfx.h"
#include <math.h>

Weapon::Weapon()
{
}

Weapon Weapon::getWeapon(WeaponType t) // Static, use this instead of ctor
{
    Weapon newWeapon;
    switch (t)
    {
    case WeaponType::eUsp45:
        newWeapon.sprites[eIdleSprite] = ResLoader::getInstance()->getSprite(sUsp45);
        newWeapon.sprites[eShootSprite] = ResLoader::getInstance()->getSprite(sUsp45Shoot);
        newWeapon.sprites[eReloadSprite] = ResLoader::getInstance()->getSprite(sUsp45Reload);
        for (auto s : newWeapon.sprites)
        {
            newWeapon.sprites[s.first].origin = Vector2f(24, 24);
            newWeapon.sprites[s.first].isLoop = false;
            newWeapon.sprites[s.first].imageSpeed = 12;
        }
        newWeapon.sprites[eReloadSprite].imageSpeed = 16;
        newWeapon.sounds[ShootSound] = ResLoader::getInstance()->getSound(sdUsp45Shoot);
        newWeapon.barrelOffset = 20;
        newWeapon.barrelHeight = 4;
        newWeapon.recoilPerShot = 12.5;
        break;
    }
    return newWeapon;
}

void Weapon::update(Time t)
{
    // Sprite animation
    sprites[currentSprite].updateImage(t);

    // Sprite animation end : sprite after shoot or reload
    if (sprites[currentSprite].isOver)
    {
        if (currentSprite == eShootSprite ||
                currentSprite == eReloadSprite ||
                currentSprite == eDeploySprite)
        {
            if (currentSprite == eReloadSprite)
            {
                ammoMag = ammoPerMag;
                isReloading = false;
            }
            currentSprite = eIdleSprite;
        }
    }

    // Weapon direction
    bool isFacingRight = cos(VTools::degToRad(angle)) > 0;
    sprites[currentSprite].imageYScale = (isFacingRight ? 1 : -1);
    angle += recoil * (isFacingRight? -1 : 1); // Apply recoil on base direction
    sprites[currentSprite].angle = angle;

    // Recoil management
    if (recoil > 0) {
        recoil -= recoilRecovery * t.asSeconds();
        if (recoil > maxRecoil) recoil = maxRecoil;
        if (recoil < 0) recoil = 0;
    }
}

void Weapon::draw(RenderWindow* w, float x, float y)
{
    sprites[currentSprite].draw(w, x, y);
}

bool Weapon::shoot()
{
    if (isLocalPlayer)
    {
        //cout << "IS LOCAL PLAYER" << endl;
        if (isReloading) {
            return false;
        }

        if (ammoMag <= 0)
        {
            reload();
            return false;
        }

        // Decrease ammo
        ammoMag--;
    }

    // Anim shoot sprite
    currentSprite = Weapon::eShootSprite;
    sprites[currentSprite].replay();

    // Apply recoil
    recoil += recoilPerShot;

    // Play shoot sound
    if (sounds.find(ShootSound) != sounds.end()) {
        sounds[ShootSound].play();
    }

    return true;
}

bool Weapon::reload()
{

    if (isLocalPlayer)
    {
        //cout << "reloading" << endl;
        if ((isReloading) || (ammoMag == ammoPerMag) || (ammoStock <= 0))
        {
            cout << "Cannot reload, isReloading " << isReloading << " ammoMag " << ammoMag
                 << " ammoPerMag " << ammoPerMag << " ammoStock " << ammoStock << endl;
            return false;
        }
        //cout << "reload started..." << endl;
    }

    // Anim reload sprite
    currentSprite = Weapon::eReloadSprite;
    sprites[currentSprite].replay();

    // Set is reloading to true
    isReloading = true;

    return true;
}
