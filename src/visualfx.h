#ifndef VISUALFX_H
#define VISUALFX_H

#include "gameobject.h"
#include "animsprite.h"

class VisualFX : public GameObject
{
public:
    VisualFX(GameRoom* r);
    void init(SpriteIndex, float angle = 0);

    virtual void update(Time);
    virtual void draw(RenderWindow*);
};

#endif // VISUALFX_H
