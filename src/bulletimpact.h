#ifndef BULLETIMPACT_H
#define BULLETIMPACT_H

#include "gameobject.h"

class BulletImpact : public GameObject
{
    friend class GameRoom;
public:
    BulletImpact(GameRoom*);
    virtual void update(Time);
    virtual void draw(RenderWindow *);

    float alpha = 1;
};

#endif // BULLETIMPACT_H
