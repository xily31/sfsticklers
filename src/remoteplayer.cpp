#include "remoteplayer.h"
//#include <QDebug>
#include "bullet.h"
#include "block.h"
#include "spawnpoint.h"
#include "stdlib.h"
#include <iostream>

RemotePlayer::RemotePlayer(GameRoom* r, SocketHolder* s)
{
    this->currentRoom = r;
    this->socketHolder = s;
    tcpSocket = &(socketHolder->tcpSocket);
    tcpSocket->setBlocking(false);
    udpSocket.setBlocking(false);
    ResLoader* rl = ResLoader::getInstance();
    sprites[sStand] = rl->getSprite(sStand);
    sprites[sRun] = rl->getSprite(sRun);
    sprites[sRunBack] = rl->getSprite(sRunBack);
    sprites[sJump] = rl->getSprite(sJump);
    sprites[sFall] = rl->getSprite(sFall);
    sprites[sDeath] = rl->getSprite(sDeath);
    spriteIndex = sStand;

    // Define the dynamic body. We set its position and call the body factory.
    /*b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(0.0f, 4.0f);
    bodyDef.fixedRotation = true;
    body = r->world->CreateBody(&bodyDef);
	body->SetGravityScale(0); // <-- let clients simulate gravity, fixes slope "shaking" (?!)

    // Box fixture
    b2PolygonShape dynamicBox;
    dynamicBox.SetAsBox(.25f, .255f);
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &dynamicBox;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.0f;
    fixtureDef.filter.categoryBits = B2dFamily::eWorldDynamic;
    fixtureDef.filter.maskBits = B2dFamily::eWorldStatic;
    body->CreateFixture(&fixtureDef);
    body->SetUserData(this);
    // Circle fixture, bottom
    b2CircleShape dynamicCircle;
    dynamicCircle.m_radius = .25f;
    dynamicCircle.m_p.y = -.25f;
    b2FixtureDef circleFixture;
    circleFixture.friction = 0.f;
    circleFixture.filter.categoryBits = B2dFamily::eWorldDynamic;
    circleFixture.filter.maskBits = B2dFamily::eWorldStatic;
    circleFixture.shape = &dynamicCircle;
    body->CreateFixture(&circleFixture);
    // Circle fixture, top
    b2CircleShape topCircleShape;
    topCircleShape.m_radius = .25f;
    topCircleShape.m_p.y = +.25f;
    b2FixtureDef topCircleFixture;
    topCircleFixture.friction = 0.f;
    topCircleFixture.shape = &topCircleShape;
    topCircleFixture.filter.categoryBits = B2dFamily::eWorldDynamic;
    topCircleFixture.filter.maskBits = B2dFamily::eWorldStatic;
    body->CreateFixture(&topCircleFixture);*/
}

RemotePlayer::~RemotePlayer()
{
    //currentRoom->world->DestroyBody(body);
    delete socketHolder;
}

void RemotePlayer::update(Time t)
{
    // Receive tcp data if available
    Packet receivePacket;
    if (tcpSocket->receive(receivePacket) == UdpSocket::Done)
    {
        handleTcpPacket(receivePacket);
    }

    // Receive udp data if available
    IpAddress ip = tcpSocket->getRemoteAddress();
    unsigned short receivePort = udpReceivePort;
    if (udpSocket.receive(receivePacket, ip, receivePort) == UdpSocket::Done)
    {
        handleUdpPacket(receivePacket);
    }

    // Set movement from player input
    applyMovement();

    // Move according to computed Box2D data
    position.x = 0;//body->GetPosition().x*PPM-PPM/2;
    position.y = 0;//-body->GetPosition().y*PPM-PPM/2;

    // Animate current sprite
    sprites[spriteIndex].updateImage(t);

    // Kill player when he fell out of the map (y > map_height)
    if (position.y > currentRoom->height && !isDead)
    {
        cout << "Player " << mpId << " fell away from da map, yo." << endl;
        health = 0;
    }

    // Kill player if health is <= 0
    if (health <= 0 && !isDead)
    {
        die();
    }

    // When dead, count time before respawn
    if (isDead)
    {
        // If respawn countdown is elapsed, tell player he's respawned
        if (respawnCounter >= (GAME_RESPAWNTIME-1))
        {
            respawn();
        }
        else // Otherwise, keep counting until able to respawn
        {
            respawnCounter += t.asSeconds();
            //cout << "RespawnCounter" << respawnCounter;
        }
    }
}

// Send death packet
void RemotePlayer::die()
{
    if (isDead) return;
    cout << "Player " << mpId << " dieded" << endl;
	isGoingLeft = false;
	isGoingRight = false;
    isDead = true;
    /*body->SetActive(false);
    body->SetAwake(false);*/

    short sendId = NETID_DEATH;
    short sendMpId = mpId;
    Packet sendPacket;
    sendPacket << sendId << sendMpId;
    sendTcp(sendPacket);
    sendToOthers(sendPacket);

    // Update health to 0 since we're dead.
    health = 0;
    healthUpdate();
}

// Send respawn packet (with new position)
void RemotePlayer::respawn()
{
    if (!isDead) return;
    cout << "Respawning player " << mpId << " after " << respawnCounter << " seconds" << endl;
    isDead = false;
    health = 100;
    respawnCounter = 0;

    /*body->SetActive(true);
    body->SetAwake(true);*/

    vector<Spawnpoint*> spawns = currentRoom->getAllObjects<Spawnpoint>();
    int min = 0, max = spawns.size()-1;
    int spawnIndex = min + (rand() % (int)(max - min + 1));
    Spawnpoint* s = spawns.at(spawnIndex);
    short sendId = NETID_SPAWN;
    short sendMpId = mpId;
    float sendX = s->getPosition().x;
    float sendY = s->getPosition().y;
    //body->SetTransform(b2Vec2(sendX/PPM, -1*sendY/PPM), 0);
    Packet sendPacket;
    sendPacket << sendId << sendMpId << sendX << sendY;
    sendTcp(sendPacket);
    sendToOthers(sendPacket);

    // Update health to 100 since we're alive
    health = 100;
    healthUpdate();
}

void RemotePlayer::handleTcpPacket(Packet receivePacket)
{
    short mId;
    receivePacket >> mId;
    switch (mId)
    {
    case NETID_TCPQUIT: // If player has quit, tell others he's not around anymore (TODO : detect if timed out)
    {
        cout << "Client disconnected, id " << mpId << ", udpPort " << udpReceivePort << endl;
        isDisconnected = true;
        Packet quitPacket;
        short quitPacketId = NETID_PLAYERQUIT;
        short quitMpId = mpId;
        quitPacket << quitPacketId << quitMpId;
        sendToOthers(quitPacket);
        return;
        break;
    }

    case NETID_REQMAP:
    {
        // Map requested : dimensions, name (TBC)
        short sendId = NETID_REQMAP;
        float sendWidth = currentRoom->width;
        float sendHeight = currentRoom->height;
        cout << "Client asking for map, sending " << sendWidth << " " << sendHeight << endl;
        Packet sendPacket;
        sendPacket << sendId << sendWidth << sendHeight;
        sendTcp(sendPacket);
        break;
    }

    case NETID_REQMAPCONTENT:
    {
        // Map content requested : blocks
        cout << "Client asking for map content..." << endl;
        vector<Block*> blocks = currentRoom->getAllObjects<Block>();
        uint blockCount = 0;
        for (auto it=blocks.begin(); it!=blocks.end(); ++it) // TODO : send all blocks in one single TCP packet
        {
            Block* b = *it;
            short sendId = NETID_NEWBLOCK;
            //b2PolygonShape* blockShape = (b2PolygonShape*)b->body->GetFixtureList()[0].GetShape();
            //short sendPointCount = blockShape->m_count;
            Packet sendPacket;
            //sendPacket << sendId << sendPointCount;
            /*for (int i=0; i<sendPointCount; ++i)
            {
                float sendX = blockShape->m_vertices[i].x;
                float sendY = blockShape->m_vertices[i].y;
                sendPacket << sendX << sendY;
            }*/
            sendTcp(sendPacket);
            blockCount++;
        }

        // Map players
        for (auto it=server->remotePlayers.begin(); it!=server->remotePlayers.end(); ++it)
        {
            RemotePlayer* r = *it;
            if (r == this) continue;
            short sendId = NETID_PLAYERJOIN;
            short sendMpId = r->mpId;
            Packet sendPacket;
            sendPacket << sendId << sendMpId;
            sendTcp(sendPacket);
        }

        // Once every map item is sent, tell client it's ready for playing
        short sendId = NETID_REQMAPCONTENT;
        Packet sendPacket;
        sendPacket << sendId;
        sendTcp(sendPacket);
        break;
    }

    case NETID_PLAYERHIT:
    {
        short rcvVictimId;
        float rcvHitYOffset;
        float rcvHitAngle;
        uint rcvBulletId;
        receivePacket >> rcvVictimId >> rcvBulletId >> rcvHitYOffset >> rcvHitAngle;
        int damage = 0;
        if (rcvHitYOffset >= GAME_HEADSHOTOFFSET) {
            damage = 20;
        } else {
            damage = 60;
        }
        cout << "Player " << mpId <<
                " has hit player " << rcvVictimId <<
                " with bullet n° " << rcvBulletId <<
                " with hitYOffset " << rcvHitYOffset <<
                " dealing " << damage << " damage " << endl;
        RemotePlayer* r = server->getRemotePlayerByMpId(rcvVictimId);
        if (r) {
            r->health -= damage;
            r->healthUpdate();
        }

        Packet sendPacket;
        short sendId = NETID_PLAYERHIT;
        sendPacket << sendId << mpId << rcvVictimId << rcvBulletId << rcvHitYOffset << rcvHitAngle;
        sendToOthers(sendPacket);

        vector<Bullet*> allBullets = currentRoom->getAllObjects<Bullet>();
        for (Bullet* b : allBullets) {
            if (b) {
                if (b->mpId == rcvBulletId && b->shooterMpId == mpId) {
                    b->destroyLater = true;
                }
            }
        }

        break;
    }

    }
}

void RemotePlayer::healthUpdate()
{
    if (health < 0) health = 0;
    short sendId = NETID_HEALTH;
    int sendHealth = health;
    Packet sendPacket;
    sendPacket << sendId << sendHealth;
    sendTcp(sendPacket);
}

void RemotePlayer::handleUdpPacket(Packet receivePacket)
{
    int16_t messageId;
    receivePacket >> messageId;
    switch (messageId)
    {
    case NETID_MAINPACKET:
    {
        int16_t netCom;
        float rcvX, rcvY, rcvXSpeed, rcvYSpeed;
        receivePacket >> netCom >> rcvX >> rcvY >> rcvXSpeed >> rcvYSpeed;
        //body->SetTransform(b2Vec2(rcvX, rcvY), 0);
        //body->SetLinearVelocity(b2Vec2(rcvXSpeed, rcvYSpeed));
        cout << "netcom : " << netCom << ", x : " << rcvX << ", y : " << rcvY << ", sX : " << endl;
        handleNetCom(netCom);

        Packet sendPacket;
        int16_t sendMpId = mpId;
        float sendX = rcvX;
        float sendY = rcvY;
        float sendXSpeed = rcvXSpeed;
        float sendYSpeed = rcvYSpeed;
        sendPacket << messageId << sendMpId << netCom << sendX << sendY << sendXSpeed << sendYSpeed;
        sendToOthers(sendPacket, true);
        break;
    }

    case NETID_GRAPHICSTATE:
    {
        int32_t intSpriteIndex;
        int32_t intWeaponSpriteIndex;
        bool isTurnedRight;
        int16_t weaponAngle;
        SpriteIndex previousSpriteIndex = spriteIndex;
        receivePacket >> intSpriteIndex >> isTurnedRight >> weaponAngle >> intWeaponSpriteIndex;
        setSpriteIndex(static_cast<SpriteIndex>(intSpriteIndex));
        if (spriteIndex == sDeath && previousSpriteIndex != sDeath) {
            sprites[spriteIndex].replay();
        }
        sprites[spriteIndex].imageXScale = (isTurnedRight? 1 : -1);
        //weaponDirection = weaponAngle; // TODO : show arms and update angle

        Packet sendPacket;
        int16_t sendMpId = mpId;
        sendPacket << messageId << sendMpId << intSpriteIndex << isTurnedRight << weaponAngle << intWeaponSpriteIndex;
        sendToOthers(sendPacket, true);
        break;
    }

    case NETID_SHOT:
    {
        float newX, newY, newVelocity, newDirection;
        uint newBulletMpId;
        receivePacket >> newBulletMpId >> newX >> newY >> newVelocity >> newDirection;

        Packet sendPacket;
        short sendPacketId = NETID_SHOT;
        short sendMpId = mpId;
        float sendX = newX, sendY = newY, sendVelocity = newVelocity, sendDirection = newDirection;
        sendPacket << sendPacketId << sendMpId << newBulletMpId << sendX << sendY << sendVelocity << sendDirection;
        sendToOthers(sendPacket, true);
        Bullet* b = currentRoom->createGameObject<Bullet>();
        b->mpId = newBulletMpId;
        b->shooterMpId = mpId;
        b->setPosition(Vector2f(newX, newY));
        b->setMotion(newDirection, newVelocity);
        break;
    }
    }
}

void RemotePlayer::handleNetCom(int16_t pNetCom)
{
    switch (pNetCom)
    {
    case NETCOM_GOLEFT: {
        isGoingLeft = true;
        break;
    }

    case NETCOM_STOPGOLEFT: {
        isGoingLeft = false;
        break;
    }

    case NETCOM_GORIGHT: {
        isGoingRight = true;
        break;
    }

    case NETCOM_STOPGORIGHT: {
        isGoingRight = false;
        break;
    }

    case NETCOM_JUMP: {
        /*b2Vec2 velocity = body->GetLinearVelocity();
        velocity.y = 20;
        body->SetLinearVelocity(velocity);*/
        isJumping = true; // isJumping not used yet
        break;
    }

    case NETCOM_STOPJUMP: {
        isJumping = false;
        break;
    }

    default: {
        break;
    }
    }
}

/*b2Vec2 RemotePlayer::computeRunReaction(const b2Vec2& checkVector)
{
    b2Vec2 ret(0, 0);
    float lowestFraction = 1;
    for (b2Body* b = currentRoom->world->GetBodyList(); b; b = b->GetNext()) {
        if (b != body)
        {
            for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) // TODO : check only blocks in the vicinity
            {
                b2RayCastInput ri;
                b2RayCastOutput ro;
                ri.maxFraction = 1;
                ri.p1 = body->GetPosition() + b2Vec2(0, -.45); // Check from bottom of the player
                ri.p2 = ri.p1 + checkVector;
                if (f->RayCast(&ro, ri, 0))
                {
                    float fraction = ro.fraction;
                    if (fraction < lowestFraction)
                    {
                        ret = ro.normal;
                        lowestFraction = fraction;
                    }
                }
            }
        }
    }
    return ret;
}*/

void RemotePlayer::applyMovement()
{
	if (isGoingLeft && !isGoingRight) // TODO : refactor from Player
    {
        float runRatio = 1;
        /*b2Vec2 runReaction = computeRunReaction(b2Vec2(-.5, 0));
        if (abs(runReaction.x) > abs(runReaction.y))
            runRatio = 0;
        if (runRatio != 0)
        {
            b2Vec2 velocity = body->GetLinearVelocity();
            velocity.x = -10;
            body->SetLinearVelocity(velocity);
        }*/
    }

    if (isGoingRight && !isGoingLeft)
    {
        float runRatio = 1;
        /*b2Vec2 runReaction = computeRunReaction(b2Vec2(.5, 0));
        if (abs(runReaction.x) > abs(runReaction.y))
            runRatio = 0;
        if (runRatio != 0)
        {
            b2Vec2 velocity = body->GetLinearVelocity();
            velocity.x = 10;
            body->SetLinearVelocity(velocity);
        }*/
	}
}

void RemotePlayer::draw(RenderWindow * w)
{
    sprites[spriteIndex].draw(w, position.x, position.y);

    /*if (drawDebug)
    {
        for (b2Fixture* f = body->GetFixtureList(); f; f = f->GetNext())
        {
            b2PolygonShape* b2PolyShape = dynamic_cast<b2PolygonShape*>(f->GetShape());
            if (b2PolyShape)
            {
                sf::ConvexShape sfPolygon;
                sfPolygon.setPointCount(b2PolyShape->m_count);
                for (int i=0; i<b2PolyShape->m_count; i++)
                {
                    b2Vec2 point = b2PolyShape->m_vertices[i];
                    b2Vec2 absPoint = body->GetWorldPoint(point);
                    Vector2f pointPosition(
                                absPoint.x*PPM,
                                -absPoint.y*PPM
                                );
                    sfPolygon.setPoint(i, pointPosition);
                }
                sfPolygon.setFillColor(Color(255, 255, 255, 128));
                sfPolygon.setOutlineColor(Color::Black);
                sfPolygon.setOutlineThickness(1);
                w->draw(sfPolygon);
            }
            else
            {
                b2CircleShape* b2Circle = dynamic_cast<b2CircleShape*>(f->GetShape());
                if (b2Circle)
                {
                    float r = b2Circle->m_radius;
                    sf::CircleShape sfCircle = sf::CircleShape(r*PPM);
                    sfCircle.setOrigin(r*PPM, r*PPM);
                    sfCircle.setPosition(
                                PPM*(body->GetPosition().x + b2Circle->m_p.x),
                                -PPM*(body->GetPosition().y + b2Circle->m_p.y)
                                     );
                    sfCircle.setFillColor(Color(255, 255, 255, 128));
                    sfCircle.setOutlineColor(Color::Black);
                    sfCircle.setOutlineThickness(1);
                    w->draw(sfCircle);
                }
            }
        }
    }*/
}

void RemotePlayer::sendTcp(Packet p)
{
    tcpSocket->send(p);
}

void RemotePlayer::sendUdp(Packet p)
{
    IpAddress ip(remoteIp);
    unsigned short port = udpSendPort;//NET_UDPCLIENTPORT;
    if (udpSocket.send(p, ip, port) != UdpSocket::Done)
    {
//        cout << "unable to send udp to" << QString::fromStdString(ip.toString()) << port;
    } else {
//        cout << "sent udp to" << QString::fromStdString(ip.toString()) << port;
    }
}

void RemotePlayer::sendToOthers(Packet p, bool isUdp) // isUdp default value is false
{
    if (server == NULL) return;
    vector<RemotePlayer*> otherRemotePlayers = server->remotePlayers;
    for (auto it=otherRemotePlayers.begin(); it!=otherRemotePlayers.end(); ++it)
    {
        RemotePlayer* r = *it;
        if (r->mpId == this->mpId || r->mpId == -1) continue;
        if (!isUdp) // Send tcp
        {
            r->sendTcp(p);
        }
        else // Send udp
        {
            r->sendUdp(p);
        }
    }
}
