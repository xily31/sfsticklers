#include "resloader.h"
//#include <QDebug>
//#include <QFile>
#include "gameroom.h"
#include "block.h"
#include "spawnpoint.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

ResLoader* ResLoader::instance = NULL;

ResLoader::ResLoader()
{
    loadSounds();
    loadSprites();
}

// Loads sounds
void ResLoader::loadSounds()
{
    SoundBuffer usp45Shoot, die, clipout, clipin, lock, hitFlesh, headshot;
    usp45Shoot.loadFromFile("res/sounds/sdUsp45.wav");
    die.loadFromFile("res/sounds/sdDie.wav");
    clipout.loadFromFile("res/sounds/sdUsp45Clipout.wav");
    clipin.loadFromFile("res/sounds/sdUsp45Clipin.wav");
    lock.loadFromFile("res/sounds/sdUsp45Lock.wav");
    hitFlesh.loadFromFile("res/sounds/sdHitFlesh.wav");
    headshot.loadFromFile("res/sounds/sdBrains.wav");

    sounds[sdUsp45Shoot] = usp45Shoot;
    sounds[sdDie] = die;
    sounds[sdClipOut] = clipout;
    sounds[sdClipIn] = clipin;
    sounds[sdLock] = lock;
    sounds[sdHitFlesh] = hitFlesh;
    sounds[sdHeadshot] = headshot;
}

// Loads and stores sprites in the sprites[] array
void ResLoader::loadSprites()
{
    AnimSprite standSprite, runSprite, runBackSprite, jumpSprite, fallSprite, deathSprite,
            usp45Sprite, usp45ShootSprite, usp45ReloadSprite,
            bulletSprite, impactSprite, muzzleFlashSprite, bloodExpSprite,
            bloodSprite, gibsSprite;

    // Particles sprites
    bloodSprite.addImagesFromTileSet("res/sprites/particles.png", 8, 0, 1, 1);
    gibsSprite.addImagesFromTileSet("res/sprites/particles.png", 8, 0, 0, 5);

    // Stickman sprites
    standSprite.addImagesFromTileSet("res/sprites/stickman.png", 32, 0, 4, 1);
    runSprite.addImagesFromTileSet("res/sprites/stickman.png", 32, 0, 0, 5);
    runBackSprite.addImagesFromTileSet("res/sprites/stickman.png", 32, 0, 5, 5);
    jumpSprite.addImagesFromTileSet("res/sprites/stickman.png", 32, 0, 1, 1);
    fallSprite.addImagesFromTileSet("res/sprites/stickman.png", 32, 0, 2, 1);
    deathSprite.addImagesFromTileSet("res/sprites/stickman.png", 32, 0, 3, 7);

    // Weapon sprites
    usp45Sprite.addImagesFromTileSet("res/sprites/weapons.png", 48, 0, 0, 1);
    usp45ShootSprite.addImagesFromTileSet("res/sprites/weapons.png", 48, 0, 1, 4);
    usp45ReloadSprite.addImagesFromTileSet("res/sprites/weapons.png", 48, 0, 2, 16);
    usp45ReloadSprite.addImagesFromTileSet("res/sprites/weapons.png", 48, 0, 3, 4);
    usp45ReloadSprite.animSounds[1] = getSound(sdClipOut);
    usp45ReloadSprite.animSounds[8] = getSound(sdClipIn);
    usp45ReloadSprite.animSounds[13] = getSound(sdLock);

    // Effects & projectiles sprites
    bulletSprite.addImagesFromTileSet("res/sprites/weapons.png", 48, 1, 0, 1);
    muzzleFlashSprite.addImagesFromTileSet("res/sprites/weapons.png", 48, 0, 4, 3);
    muzzleFlashSprite.origin = Vector2f(0, 24);
    impactSprite.addImagesFromTileSet("res/sprites/weapons.png", 48, 0, 5, 5);
    impactSprite.origin = Vector2f(47, 24);
    bloodExpSprite.addImagesFromTileSet("res/sprites/weapons.png", 48, 0, 6, 4);
    bloodExpSprite.origin = Vector2f(47, 24);

    // Store loaded sprites in sprites[] array
    sprites[sStand] = standSprite;
    sprites[sRun] = runSprite;
    sprites[sJump] = jumpSprite;
    sprites[sFall] = fallSprite;
    sprites[sRunBack] = runBackSprite;
    sprites[sDeath] = deathSprite;
    sprites[sDeath].isLoop = false;
    sprites[sDeath].lastTopImageIndex = sprites[sDeath].images.size() - 1;
    sprites[sUsp45] = usp45Sprite;
    sprites[sBullet] = bulletSprite;
    sprites[sUsp45Shoot] = usp45ShootSprite;
    sprites[sUsp45Reload] = usp45ReloadSprite;
    sprites[sImpact] = impactSprite;
    sprites[sMuzzleFlash] = muzzleFlashSprite;
    sprites[sBloodExp] = bloodExpSprite;
    sprites[sGibs] = gibsSprite;
    sprites[sBlood] = bloodSprite;

    cout << "Loaded sprites" << endl;
}

AnimSprite ResLoader::getSprite(SpriteIndex s)
{
    return sprites[s];
}

Sound ResLoader::getSound(SoundIndex s)
{
    Sound newSound;
    newSound.setBuffer(sounds[s]);
    return newSound;
}

GameRoom* ResLoader::loadMap(string fileName, RenderWindow* w)
{
    fstream mapFile;
    mapFile.open(fileName, ios_base::out | ios_base::in);
    if (!mapFile.is_open()) {
        cout << "Map file does not exist" << fileName;
        return NULL;
    }
    else
    {
		cout << "Reading map from file " << fileName << endl;
        GameRoom* r = new GameRoom(w);
        vector<string> mapLines;
        string line;

        // Read map header (width, height)
        r->width = (getline(mapFile, line)? atof(line.c_str()) : 0);
        r->height = (getline(mapFile, line)? atof(line.c_str()) : 0);

        if (r->width == 0 || r->height == 0) {
            cout << "Error reading map dimensions, canceling loading" << endl;
            return NULL;
        }

        // Read map content
        while (getline(mapFile, line))
        {
			cout << "Read line " << line << endl;
            if (line == "block")
            {
				cout << "Reading block" << endl;
                vector<sf::Vector2f> blockVertices;
                while (getline(mapFile, line)) {
                    if (line == "endblock") break;
                    float x = atof(line.c_str());
                    float y = (getline(mapFile, line)? atof(line.c_str()):0);
                    blockVertices.push_back(Vector2f(x*PPM, y*PPM));
                }
                Block* b = r->createGameObject<Block>();
                b->initialize(blockVertices);
				cout << "Read block" << endl;
            }
            else if (line == "spawn")
            {
				cout << "Reading spawn" << endl;
                float x = (getline(mapFile, line)? atof(line.c_str()):0);
                float y = (getline(mapFile, line)? atof(line.c_str()):0);
                Spawnpoint* s = r->createGameObject<Spawnpoint>();
                s->setPosition(Vector2f(x, y));
				cout << "Read spawn" << endl;
            }
        }
        mapFile.close();
        return r;
    }
    return NULL;
}
